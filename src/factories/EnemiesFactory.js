import gameState from '../state/game'
import Crabes from '../sprites/enemies/Crabes'
import Guepes from '../sprites/enemies/Guepes'
import Jumpers from '../sprites/enemies/Jumpers'
import Octopus from '../sprites/enemies/Octopus'
import RhinoBeetles from '../sprites/enemies/RhinoBeetles'

export default class EnemiesFactory {
  static make (enemyClass, enemiesData) {
    const classesMapping = {
      Crabes,
      Guepes,
      Jumpers,
      Octopus,
      RhinoBeetles,
    }

    return enemiesData.map((enemyData) => {
      const enemy = new classesMapping[enemyClass](
        gameState.scene,
        enemyData.x,
        enemyData.y - 16,
        {
          key: enemyData.properties.key,
          life: enemyData.properties.life,
          damage: enemyData.properties.damage,
        })
      enemy.animate(enemyData.properties.key, true)
      return enemy
    })
  }
}
