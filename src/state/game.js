export default {
  scene: null,
  levelStartTimestamp: null,
  camPosition: {},
  displayPowerUpMsg: false,
  // Useful
  map: null,
  tileset: null,
  player: null,
  mask: null,
  round: null,
  isTheEnd: null,
  playerHurt: null,
  playerDead: null,
  giveLife: null,
  // EnemiesManager related
  enemyGroup: null,
  explodeSprite: null,
  // MapManager related
  backLayer: null,
  middleLayer: null,
  middleLayer2: null, // ? used nowhere
  statue: null,
  eau: null,
  solLayer: null,
  frontLayer: null,
  // EnvironmentManager related
  elevatorGroup: null,
  lavaGroup: null,
  doorGroup: null,
  boss1Door: null,
  lavaRise: null,
  lavaRiseFlag: null,
  onSismic: null,
  // MusicManager related
  ambient1: null,
  ambient2: null,
  ambient3: null,
  waterAmbientMusic: null,
  // PowerUpsManager related
  powerups: null,
  giveLifeGroup: null,
  powerUpText: null,
  // MessagesManager related
  modalText: null,
  // BossManager related
  boss1started: null,
  boss1BattlePreventSave: null,
  bossMusic: null,
  bossFinalReady: null,
  bossFinalStarted: null,
  bossFinalBattlePreventSave: null,
  bossFinal: null,
  boss1: null,
  boss1dead: null,
}
