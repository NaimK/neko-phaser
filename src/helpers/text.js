import { GAME_WIDTH, GAME_HEIGHT } from '../constants/sizes'
import { WHITE } from '../constants/colors'

const font = 'atomic'

export const addText = (context, text, {
  level = 'default',
  left = GAME_WIDTH / 2,
  top = GAME_HEIGHT / 2,
  tint = WHITE,
  center = true,
} = {}) => {
  let size

  switch (level) {
    case 'hero':
      size = 50
      break
    case 'lg':
      size = 35
      break
    case 'default':
      size = 20
      break
    case 'sm':
      size = 15
      break
    default:
      size = 20
      break
  }

  const bmpText = context.add
    .bitmapText(left, top, font, text, size, 1)
    .setTint(tint)

  if (center) {
    bmpText.setOrigin(0.5, 0.5)
  }

  return bmpText
}

export const typewriteText = (context, text, {
  level = 'default',
  left = GAME_WIDTH / 2,
  top = GAME_HEIGHT / 2,
  tint = WHITE,
  interval = 75,
  callback = null,
} = {}) => {
  let size

  switch (level) {
    case 'hero':
      size = 50
      break
    case 'lg':
      size = 35
      break
    case 'default':
      size = 20
      break
    case 'sm':
      size = 15
      break
    default:
      size = 20
      break
  }

  let count = 0

  const typedText = context.add
    .bitmapText(left, top, font, '', size, 1)
    .setOrigin(0.5, 0.5)

  context.time.addEvent({
    delay: interval,
    repeat: text.length - 1,
    callback: () => {
      if (text[count] === '|') {
        typedText.text += '\n'
      } else {
        typedText.text += text[count]
      }
      count += 1
      if (callback) {
        callback()
      }
    },
  })

  return typedText
}
