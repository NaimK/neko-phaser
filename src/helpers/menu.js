import { GAME_WIDTH, GAME_HEIGHT } from '../constants/sizes'

export const addBackground = (context) => {
  return context.add.image(0, 0, 'menuBackground')
    .setOrigin(0, 0)
    .setDisplaySize(GAME_WIDTH, GAME_HEIGHT)
}

export const addCursor = (context, size = 50) => {
  return context.add
    .image(0, 0, 'menuCursor')
    .setOrigin(0, 0)
    .setDisplaySize(size, size)
}
