import { DEFAULT_KEYS } from '../constants/default'

export const getKeys = () => {
  if (!localStorage.getItem('userKeys')) {
    localStorage.setItem('userKeys', JSON.stringify(DEFAULT_KEYS))
  }
  return JSON.parse(localStorage.getItem('userKeys'))
}

export const updateKeys = (newKeys) => {
  localStorage.setItem('userKeys', JSON.stringify(newKeys))
}

export const getSavedGame = () => {
  return JSON.parse(localStorage.getItem('k438b'))
}

export const removeSavedGame = () => {
  localStorage.removeItem('k438b')
  localStorage.removeItem('d')
  localStorage.removeItem('e')
  localStorage.removeItem('time')
}
