export const countPlayedTime = (levelStartTimestamp) => {
  const levelEndTimestamp = new Date().getTime()
  const result = levelEndTimestamp - levelStartTimestamp
  let elapsedTime = localStorage.getItem('time')
  elapsedTime = JSON.parse(elapsedTime)
  localStorage.setItem('time', elapsedTime + result)
  return new Date().getTime()
}
