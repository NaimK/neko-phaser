export const tweenLoopWait = (context, target, {
  duration = 1500,
  delay = 1000,
  repeat = -1,
  yoyo = true,
} = {}, onStart) => {
  context.tweens.add({
    targets: target,
    duration,
    delay,
    repeat,
    yoyo,
    alpha: {
      getStart: () => 0,
      getEnd: () => 1,
    },
    onStart,
  })
}
