import { Scene } from 'phaser'
import { addBackground, addCursor } from '../helpers/menu'
import { addText } from '../helpers/text'
import { getKeys as getStoredUserKeys, updateKeys } from '../helpers/user'
import { DEFAULT_KEYS } from '../constants/default'
import text from '../assets/text/en.json'
import { GAME_WIDTH, GUTTER } from '../constants/sizes'

export default class MainMenuOptions extends Scene {
  #menuItems
  #menuItemsSpacing
  #menuItemsX
  #menuItemsStartY
  #menuItemsNextY
  #storedUserKeys
  #newUserKeys
  #activeMenuItemIndex
  #menuItemNextId
  #cursor
  #cursorSize
  #navigationEnabled

  constructor () {
    super('mainMenuOptions')
  }

  create () {
    this.#setInitialValues()
    this.#storedUserKeys = getStoredUserKeys()
    this.#newUserKeys = this.#storedUserKeys
    this.#addAssets()
    this.#addTexts()
    this.#displayMenuItems()
    this.#setCursorPosition(this.#menuItemsStartY)
    this.#bindListeners()
  }

  #setInitialValues () {
    this.#menuItems = []
    this.#menuItemsSpacing = 30
    this.#menuItemsX = 100
    this.#menuItemsStartY = 75
    this.#menuItemsNextY = 75
    this.#storedUserKeys = null
    this.#activeMenuItemIndex = 0
    this.#menuItemNextId = 0
    this.#cursor = null
    this.#cursorSize = 25
    this.#navigationEnabled = true
  }

  #addAssets () {
    addBackground(this)
    this.#cursor = addCursor(this, this.#cursorSize)
  }

  #addTexts () {
    addText(this, text.main_menu_options.title, { top: GUTTER })
    addText(this, text.main_menu_options.subtitle, { level: 'sm', top: GUTTER * 2 }).setAlpha(0.5)
  }

  #displayMenuItems () {
    const keys = this.#storedUserKeys ?? DEFAULT_KEYS

    Object.keys(keys).forEach((key) => {
      this.#addKeyToMenuOptions(key, text.main_menu_options[key], keys[key])
    })

    this.#menuItemsNextY += this.#menuItemsSpacing

    this.#menuItems.push({ id: this.#menuItemNextId })
    this.#menuItemNextId++

    ;['save', 'reset', 'quit'].forEach(value => {
      const label = text.main_menu_options[value]
      this.#menuItems.push({ id: this.#menuItemNextId, key: value, label })

      addText(this, text.main_menu_options[value], {
        left: this.#menuItemsX,
        top: this.#menuItemsNextY,
        center: false,
      })
      this.#menuItemsNextY += this.#menuItemsSpacing
      this.#menuItemNextId++
    })
  }

  #addKeyToMenuOptions (key, action, value) {
    const bmpLabel = addText(this, action, {
      left: this.#menuItemsX,
      top: this.#menuItemsNextY,
      center: false,
    })
    const bmpKey = addText(this, value, {
      left: this.#menuItemsX + 300,
      top: this.#menuItemsNextY,
      center: false,
    })

    this.#menuItems.push({ id: this.#menuItemNextId, key, label: action, value, bmpLabel, bmpKey })

    this.#menuItemsNextY += this.#menuItemsSpacing
    this.#menuItemNextId++
  }

  #navigate (direction) {
    if (direction === 'down' && this.#activeMenuItemIndex < this.#menuItems.length - 1) {
      this.#activeMenuItemIndex++
      if (!this.#menuItems[this.#activeMenuItemIndex].key) {
        // This is the menu spacer
        this.#activeMenuItemIndex++
      }
    } else if (direction === 'up' && this.#activeMenuItemIndex > 0) {
      this.#activeMenuItemIndex--
      if (!this.#menuItems[this.#activeMenuItemIndex].key) {
        // This is the menu spacer
        this.#activeMenuItemIndex--
      }
    }
    this.#setCursorPosition(this.#menuItemsStartY + this.#menuItemsSpacing * this.#activeMenuItemIndex)
  }

  #setCursorPosition (posY) {
    this.#cursor.setPosition(GAME_WIDTH / 20, posY)
  }

  #activateMenuItem (value) {
    switch (value.key) {
      case 'quit':
        this.scene.start('mainMenu')
        break
      case 'reset':
        this.#saveKeys(DEFAULT_KEYS)
        this.sound.play('save')
        this.scene.start('mainMenu')
        break
      case 'save':
        this.#saveKeys(this.#newUserKeys)
        this.sound.play('save')
        this.scene.start('mainMenu')
        break
      default:
        this.#assignNewKey(value)
        break
    }
  }

  #assignNewKey (value) {
    this.#navigationEnabled = false
    value.bmpKey.text = text.main_menu_options.enter_new_key

    // TODO: Actually only allow allowed keys and prevent duplicate
    this.input.keyboard.once('keydown', (e) => {
      this.#newUserKeys[value.key] = this.#formatNewKey(e.key)
      value.bmpKey.text = this.#formatNewKey(e.key)
      this.#navigationEnabled = true
    })
  }

  #saveKeys (keys) {
    updateKeys(keys)
  }

  #bindListeners () {
    this.input.keyboard.on('keydown', (event) => {
      if (!this.#navigationEnabled) return

      if (event.code === 'ArrowDown') {
        this.#navigate('down')
      } else if (event.code === 'ArrowUp') {
        this.#navigate('up')
      } else if (event.code === 'Enter') {
        this.#activateMenuItem(this.#menuItems.find(item =>
          this.#activeMenuItemIndex === item.id
        ))
      }
    })
  }

  #formatNewKey (value) {
    return value.toUpperCase()
      .replace('ARROW', '')
      .replace('SHIFTLEFT', 'SHIFT')
      .replace('SHIFTRIGHT', 'SHIFT')
      .replace('CTRLLEFT', 'CTRL')
      .replace('CTRLRIGHT', 'CTRL')
  }
}
