import { Input, Scene } from 'phaser'
import bip from '../assets/sounds/bip.ogg'
import { getKeys as getStoredUserKeys, getSavedGame, removeSavedGame } from '../helpers/user'
import { addText } from '../helpers/text'
import { addBackground, addCursor } from '../helpers/menu'
import { GAME_HEIGHT, GAME_WIDTH } from '../constants/sizes'
import text from '../assets/text/en.json'

export default class Intro extends Scene {
  #bgImage
  #keys
  #storedUserKeys
  #hasSavedGame
  #menuItems
  #menuItemsSpacing = 100
  #menuItemsStartY
  #activeMenuItemIndex = 0
  #cursor
  #cursorSize = 50

  constructor () {
    super('mainMenu')
  }

  preload () {
    this.#loadAssets()
  }

  create () {
    this.#hasSavedGame = getSavedGame()
    this.#storedUserKeys = getStoredUserKeys()
    this.#addAssets()
    this.#setKeys()
    this.#setMenu()
    this.#bindListeners()
  }

  #loadAssets () {
    this.load.audio('bip', bip)
  }

  #addAssets () {
    this.#bgImage = addBackground(this)
    this.#cursor = addCursor(this, this.#cursorSize)
  }

  #setKeys () {
    this.#keys = this.input.keyboard.addKeys({
      up: Input.Keyboard.KeyCodes[this.#storedUserKeys.up],
      down: Input.Keyboard.KeyCodes[this.#storedUserKeys.down],
      fire: Input.Keyboard.KeyCodes[this.#storedUserKeys.fire],
    })
  }

  #setMenu () {
    this.#setMenuItems()
    this.#displayMenuItems()
    this.#setCursorPosition(this.#menuItemsStartY)
  }

  #setMenuItems () {
    if (this.#hasSavedGame) {
      this.#menuItems = [
        text.main_menu.load_game,
        text.main_menu.options,
        text.main_menu.delete_game,
      ]
    } else {
      this.#menuItems = [
        text.main_menu.new_game,
        text.main_menu.options,
      ]
    }
    // Only work when menu items length is either 2 or 3
    this.#menuItemsStartY = GAME_HEIGHT / 2 - this.#menuItemsSpacing / (this.#menuItems.length === 2 ? 2 : 1)
  }

  #displayMenuItems () {
    let itemPosition = this.#menuItemsStartY

    this.#menuItems.forEach((menuItem) => {
      addText(this, menuItem, { top: itemPosition, level: 'lg' })
      itemPosition += this.#menuItemsSpacing
    })
  }

  #setCursorPosition (posY) {
    this.#cursor.setPosition(GAME_WIDTH / 5, posY - this.#cursorSize / 2)
  }

  #navigate (direction) {
    if (direction === 'down' && this.#activeMenuItemIndex < this.#menuItems.length - 1) {
      this.#activeMenuItemIndex += 1
    } else if (direction === 'up' && this.#activeMenuItemIndex > 0) {
      this.#activeMenuItemIndex -= 1
    }
    this.#setCursorPosition(this.#menuItemsStartY + this.#menuItemsSpacing * this.#activeMenuItemIndex)
  }

  #bindListeners () {
    this.input.keyboard.on('keydown', (event) => {
      if (event.code === 'ArrowDown') {
        this.#navigate('down')
      } else if (event.code === 'ArrowUp') {
        this.#navigate('up')
      } else if (event.code === 'Enter') {
        this.sound.play('bip2', { volume: 0.1 })
        this.#goToNextScene()
      }
    })
  }

  #goToNextScene () {
    let nextScene
    let data = {}

    switch (this.#menuItems[this.#activeMenuItemIndex]) {
      case text.main_menu.new_game:
        nextScene = 'level1Loader'
        break
      case text.main_menu.load_game:
        nextScene = 'level1Loader'
        data = { loadSavedGame: true }
        break
      case text.main_menu.delete_game:
        nextScene = 'mainMenu'
        removeSavedGame()
        this.sound.play('bip2', { volume: 0.1 })
        break
      case text.main_menu.options:
        nextScene = 'mainMenuOptions'
        break
      default:
        break
    }

    this.#activeMenuItemIndex = 0

    this.scene.start(nextScene, data)
  }
}
