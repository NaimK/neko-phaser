import { Scene } from 'phaser'
import logo from '../assets/images/logo.svg'
import { addText } from '../helpers/text'
import { tweenLoopWait } from '../helpers/tweens'
import { GAME_HEIGHT, GUTTER } from '../constants/sizes'
import text from '../assets/text/en.json'

export default class SplashScreen extends Scene {
  constructor () {
    super('splashScreen')
  }

  #logo = logo
  #continueText

  preload () {
    this.#loadAssets()
  }

  create () {
    this.#addLogo()
    this.#addTexts()
    this.#showAnimation()
    tweenLoopWait(this, this.#continueText, {}, () => this.#bindListeners())
  }

  #loadAssets () {
    this.load.image('logo', logo)
  }

  #addLogo () {
    this.#logo = this.add.image(this.cameras.main.centerX, this.cameras.main.centerY, 'logo')
  }

  #addTexts () {
    this.#continueText = addText(this, text.splash_screen.continue, { level: 'sm', top: GAME_HEIGHT - GUTTER }).setAlpha(0)
  }

  #showAnimation () {
    this.tweens.add({
      targets: this.#logo,
      ease: 'Sine.easeInOut',
      duration: 1000,
      repeat: 0,
      alpha: {
        getStart: () => 0,
        getEnd: () => 1,
      },
    })
  }

  #bindListeners () {
    this.input.keyboard.once('keydown', () => {
      this.scene.start('bootGame')
    })
  }
}
