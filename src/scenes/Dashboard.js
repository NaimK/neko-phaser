import { Scene } from 'phaser'
import gameState from '../state/game'

import { GAME_WIDTH } from '../constants/sizes'
import blackPixel from '../assets/images/game/blackPixel.png'
import iconMissile from '../assets/images/game/iconMissile.png'
import iconLaser from '../assets/images/game/iconLaser.png'
import iconSwell from '../assets/images/game/iconSwell.png'
import iconFullscreen from '../assets/images/game/iconFullscreen.png'

export default class Dashboard extends Scene {
  constructor () {
    super({ key: 'dashboard', active: true })
  }

  preload () {
    this.load.image('blackpixel', blackPixel)
    this.load.image('iconMissile', iconMissile)
    this.load.image('iconLaser', iconLaser)
    this.load.image('iconSwell', iconSwell)
    this.load.image('iconFullscreen', iconFullscreen)
  }

  create () {
    this.mainScene = this.scene.get('level1')

    // loading
    this.mainScene.events.on('loadingDone', () => {
      this.dashBoard = this.add.image(0, 448, 'blackpixel')
        .setOrigin(0, 0)
        .setDisplaySize(GAME_WIDTH, 64)

      this.lifeText = this.add.bitmapText(16, 448, 'atomic', 'H e a l t h')
        .setFontSize(16)

      this.Health = this.add.bitmapText(16, 464, 'atomic', '')
        .setFontSize(32)
        .setText(`${gameState.player.inventory.life}/${gameState.player.inventory.lifeEnergyBlock * 100}`)

      this.swell = this.add.image(400, 480, 'iconSwell')
        .setAlpha(0)
        .setDisplaySize(36, 40)

      this.missile = this.add.image(450, 480, 'iconMissile')
        .setAlpha(0)
        .setDisplaySize(36, 40)

      this.laser = this.add.image(500, 480, 'iconLaser')
        .setAlpha(0)
        .setDisplaySize(36, 40)

      this.fullscreenBtn = this.add.image(750, 480, 'iconFullscreen')
        .setDisplaySize(64, 64)
        .setInteractive()
        .on('pointerdown', () => {
          this.scale.toggleFullscreen()
        }, this)

      if (gameState.player.inventory.missile) {
        this.missile.setAlpha(1)
      }
      if (gameState.player.inventory.laser) {
        this.laser.setAlpha(1)
      }
      if (gameState.player.inventory.swell) {
        this.swell.setAlpha(1)
      }
    })

    this.mainScene.events.on('setHealth', (elm) => {
      this.Health.setText(`${elm.life}/${gameState.player.inventory.lifeEnergyBlock * 100}`)
    })

    this.mainScene.events.on('addEnergyPack', (elm) => {
      this.Health.setText(elm.life)
    })

    this.mainScene.events.on('addWeapon', (elm) => {
      this[elm.Weapon].setAlpha(1)
    })

    this.mainScene.events.on('selectWeapon', (elm) => {
      this.missile.clearTint()
      this.laser.clearTint()
      this.swell.clearTint()
      if (this[elm.selectedWeapon]) {
        this[elm.selectedWeapon].setTint(0xFF3B00)
      }
    })
  }
}
