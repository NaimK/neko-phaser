import { Scene } from 'phaser'

// Map
import tiles from '../../assets/environment/layers/tilesets.png'
import mapLevel1 from '../../assets/maps/mapLevel1.json'

// Player
import playerRunShoot from '../../assets/spritesheets/player/runShoot.png'
import idle from '../../assets/spritesheets/player/idle.png'
import stand from '../../assets/spritesheets/player/stand.png'
import jump from '../../assets/spritesheets/player/jump.png'
import jumpVertical from '../../assets/spritesheets/player/jumpVertical.png'
import duck from '../../assets/spritesheets/player/duck.png'
import shootUp from '../../assets/spritesheets/player/shootUp.png'
import morphingBall from '../../assets/spritesheets/player/morphingBall.png'

// Power Up
import powerupBlue from '../../assets/images/game/powerupBleu.png'
import powerupYellow from '../../assets/images/game/powerupJaune.png'
import powerupGreen from '../../assets/images/game/powerupVert.png'
import powerupRed from '../../assets/images/game/powerupRouge.png'
import powerUp from '../../assets/spritesheets/Fx/power-up.png'

// Enemies
import crabe from '../../assets/spritesheets/enemies/crab-walk.png'
import guepe from '../../assets/spritesheets/enemies/guepe.png'
import guepe2 from '../../assets/spritesheets/enemies/guepe2.png'
import jumper from '../../assets/spritesheets/enemies/jumper-idle.png'
import jumper2 from '../../assets/spritesheets/enemies/jumper-idle2.png'
import enemyExplode from '../../assets/spritesheets/Fx/enemy-death.png'
import octopus from '../../assets/spritesheets/enemies/octopus.png'
import fireballs from '../../assets/images/game/fire-ball.png'
import rhinoBeetle from '../../assets/spritesheets/enemies/rhinoBeetle.png'

// boss 1
import boss1wallfront from '../../assets/images/game/boss1wallfront.png'
import boss1Walk from '../../assets/spritesheets/enemies/boss1walk.png'
import boss1Run from '../../assets/spritesheets/enemies/boss1run.png'
import boss1Crouch from '../../assets/spritesheets/enemies/boss1crouch.png'
import boss1Attack from '../../assets/spritesheets/enemies/boss1attack.png'
import boss1Jump from '../../assets/spritesheets/enemies/boss1jump.png'
import boss1Hit from '../../assets/spritesheets/enemies/boss1hit.png'

// final boss
import bosstest from '../../assets/spritesheets/enemies/bossAttack.png'

// Various
import bullet from '../../assets/spritesheets/Fx/shot.png'
import bomb from '../../assets/images/game/bomb.png'
import laser from '../../assets/images/game/laser.png'
import impact from '../../assets/spritesheets/Fx/impact.png'
import missile from '../../assets/images/game/missile.png'
import swell from '../../assets/images/game/swell.png'
import blackPixel from '../../assets/images/game/blackPixel.png'
import lavaPixel from '../../assets/images/game/lavaPixel.png'
import elevator from '../../assets/images/game/elevator.png'
import door from '../../assets/images/game/door.png'
import head from '../../assets/images/game/head.png'
import whitePixel from '../../assets/images/game/whitePixel.png'
import lava from '../../assets/images/game/lava.png'
import lavaFall from '../../assets/images/game/lava-fall.png'
import waterFall from '../../assets/images/game/waterfall.png'
import boss1dead from '../../assets/images/game/boss1dead.png'

// parralax
import paraMiddleground from '../../assets/environment/layers/para_middleground.png'
import paraBackground from '../../assets/environment/layers/background.png'
import bgLava from '../../assets/images/game/bgLava.png'

// import sounds fx
import bulletFX from '../../assets/sounds/bullet.ogg'
import swellFX from '../../assets/sounds/swell.ogg'
import missileFX from '../../assets/sounds/missile.ogg'
import laserFX from '../../assets/sounds/laser3.ogg'
import impactFX from '../../assets/sounds/explo.ogg'
import explo2FX from '../../assets/sounds/explo2.ogg'
import enemyImpactFX from '../../assets/sounds/enemyHit.ogg'
import playerHitFX from '../../assets/sounds/playerHit.ogg'
import morphFX from '../../assets/sounds/playerHit2.ogg'
import powerUpFX from '../../assets/sounds/powerup.ogg'
import selectFX from '../../assets/sounds/select.ogg'
import jumpBoosterFX from '../../assets/sounds/jumpboost.ogg'
import getLifeFX from '../../assets/sounds/getlife2.ogg'
import runFX from '../../assets/sounds/walk.ogg'
import explo3FX from '../../assets/sounds/explo3.ogg'
import melo from '../../assets/sounds/melo1.ogg'
import playerDead from '../../assets/sounds/playerdead.ogg'
import shake from '../../assets/sounds/shake3.ogg'
import shake2 from '../../assets/sounds/shake4.ogg'
import guepeFX from '../../assets/sounds/guepe.ogg'
import grog from '../../assets/sounds/grog.ogg'

// import boss1 sounds fx
import cri1 from '../../assets/sounds/boss1/cri-001.ogg'
import cri2 from '../../assets/sounds/boss1/cri-002.ogg'
import cri3 from '../../assets/sounds/boss1/cri-003.ogg'
import cri4 from '../../assets/sounds/boss1/cri-004.ogg'
import LetsPlayWithTheDemon from '../../assets/music/LetsPlayWithTheDemon.ogg'

// import music
import ambient1 from '../../assets/music/ambient1.ogg'
import ambient2 from '../../assets/music/ambient2.ogg'
import waterAmbient from '../../assets/music/waterAmbiance.ogg'
import ambient3 from '../../assets/music/grotte.ogg'

export default class Level1Loader extends Scene {
  constructor () {
    super('level1Loader')
  }

  preload () {
    // map
    this.load.image('tiles', tiles)
    this.load.tilemapTiledJSON('mapLevel1', mapLevel1)

    // player animation
    this.load.spritesheet('playerShoot', playerRunShoot, {
      frameWidth: 38,
      frameHeight: 40,
    })
    this.load.spritesheet('idle', idle, { frameWidth: 40, frameHeight: 55 })
    this.load.spritesheet('stand', stand, { frameWidth: 40, frameHeight: 40 })
    this.load.spritesheet('duck', duck, { frameWidth: 40, frameHeight: 40 })
    this.load.spritesheet('shootUp', shootUp, {
      frameWidth: 40,
      frameHeight: 40,
    })
    this.load.spritesheet('jump', jump, { frameWidth: 40, frameHeight: 40 })
    this.load.spritesheet('jumpVertical', jumpVertical, {
      frameWidth: 40,
      frameHeight: 40,
      startFrame: 2,
      endFrame: 5,
    })
    this.load.spritesheet('morphingBall', morphingBall, {
      frameWidth: 40,
      frameHeight: 40,
    })

    // player bullets
    this.load.spritesheet('bullet', bullet, { frameWidth: 6, frameHeight: 4 })
    this.load.spritesheet('impact', impact, {
      frameWidth: 12,
      frameHeight: 12,
    })
    this.load.spritesheet('missile', missile, {
      frameWidth: 18,
      frameHeight: 10,
    })
    this.load.spritesheet('bomb', bomb, { frameWidth: 16, frameHeight: 16 })
    this.load.spritesheet('swell', swell, { frameWidth: 12, frameHeight: 12 })
    this.load.image('laser', laser)

    // power up
    this.load.spritesheet('powerupBlue', powerupBlue, {
      frameWidth: 16,
      frameHeight: 16,
    })
    this.load.spritesheet('powerupYellow', powerupYellow, {
      frameWidth: 16,
      frameHeight: 16,
    })
    this.load.spritesheet('powerupGreen', powerupGreen, {
      frameWidth: 16,
      frameHeight: 16,
    })
    this.load.spritesheet('powerupRed', powerupRed, {
      frameWidth: 16,
      frameHeight: 16,
    })
    this.load.spritesheet('powerUp', powerUp, {
      frameWidth: 23,
      frameHeight: 21,
    })

    // Enemies
    this.load.spritesheet('crabe', crabe, { frameWidth: 48, frameHeight: 32 })
    this.load.spritesheet('guepe', guepe, { frameWidth: 40, frameHeight: 47 })
    this.load.spritesheet('guepe2', guepe2, {
      frameWidth: 40,
      frameHeight: 47,
    })
    this.load.spritesheet('jumper1', jumper, {
      frameWidth: 47,
      frameHeight: 32,
    })
    this.load.spritesheet('jumper2', jumper2, {
      frameWidth: 47,
      frameHeight: 32,
    })
    this.load.spritesheet('enemyExplode', enemyExplode, {
      frameWidth: 67,
      frameHeight: 48,
    })
    this.load.spritesheet('octopus', octopus, {
      frameWidth: 28,
      frameHeight: 37,
    })
    this.load.spritesheet('fireball', fireballs, {
      frameWidth: 16,
      frameHeight: 16,
    })
    this.load.spritesheet('rhinobeetle', rhinoBeetle, {
      frameWidth: 80,
      frameHeight: 64,
    })

    // boss
    this.load.image('boss1wallfront', boss1wallfront)
    this.load.spritesheet('boss1walk', boss1Walk, {
      frameWidth: 184,
      frameHeight: 136,
    })
    this.load.spritesheet('boss1run', boss1Run, {
      frameWidth: 178,
      frameHeight: 136,
    })
    this.load.spritesheet('boss1crouch', boss1Crouch, {
      frameWidth: 185,
      frameHeight: 136,
    })
    this.load.spritesheet('boss1attack', boss1Attack, {
      frameWidth: 188,
      frameHeight: 136,
    })
    this.load.spritesheet('boss1hit', boss1Hit, {
      frameWidth: 153,
      frameHeight: 136,
    })
    this.load.spritesheet('boss1jump', boss1Jump, {
      frameWidth: 172,
      frameHeight: 179,
    })

    // final boss
    this.load.spritesheet('bossFinal', bosstest, {
      frameWidth: 384,
      frameHeight: 480,
    })

    // various map items
    // this.load.spritesheet('savestation', saveStation, { frameWidth: 40, frameHeight: 60 });
    this.load.image('head', head)
    this.load.image('elevator', elevator)
    this.load.image('door', door)
    this.load.spritesheet('lava', lava, { frameWidth: 32, frameHeight: 32 })
    this.load.spritesheet('lavaFall', lavaFall, {
      frameWidth: 16,
      frameHeight: 16,
    })
    this.load.spritesheet('waterFall', waterFall, {
      frameWidth: 16,
      frameHeight: 16,
    })
    this.load.image('blackPixel', blackPixel)
    this.load.image('whitePixel', whitePixel)
    this.load.image('lavaPixel', lavaPixel)
    this.load.image('boss1dead', boss1dead)

    // parralax
    this.load.image('para_middle', paraMiddleground)
    this.load.image('para_back', paraBackground)
    this.load.image('bgLava', bgLava)

    // sounds
    this.load.audio('bullet', bulletFX)
    this.load.audio('swell', swellFX)
    this.load.audio('missile', missileFX)
    this.load.audio('laser', laserFX)
    this.load.audio('impact', impactFX)
    this.load.audio('explo2', explo2FX)
    this.load.audio('explo3', explo3FX)
    this.load.audio('enemyHit', enemyImpactFX)
    this.load.audio('playerHit', playerHitFX)
    this.load.audio('powerUp', powerUpFX)
    this.load.audio('select', selectFX)
    this.load.audio('jumpBooster', jumpBoosterFX)
    this.load.audio('getLife', getLifeFX)
    this.load.audio('run', runFX)
    this.load.audio('morph', morphFX)
    this.load.audio('melo', melo)
    this.load.audio('playerDead', playerDead)
    this.load.audio('shake', shake)
    this.load.audio('shake2', shake2)
    this.load.audio('guepe', guepeFX)
    this.load.audio('jumpers', grog)

    // sounds boss1
    this.load.audio('cri1', cri1)
    this.load.audio('cri2', cri2)
    this.load.audio('cri3', cri3)
    this.load.audio('cri4', cri4)
    this.load.audio('LetsPlayWithTheDemon', LetsPlayWithTheDemon)
    // music
    this.load.audio('ambient1', ambient1)
    this.load.audio('ambient2', ambient2)
    this.load.audio('ambient3', ambient3)
    this.load.audio('waterAmbient', waterAmbient)
  }

  create () {
    const data = this.data.systems.settings.data
    this.scene.start('level1', data ?? null)
  }
}
