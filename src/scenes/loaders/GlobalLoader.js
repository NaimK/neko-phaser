import { Scene } from 'phaser'
import atomicsc from '../../assets/fonts/atomicsc.png'
import atomicscXML from '../../assets/fonts/atomicsc.xml'
import background from '../../assets/images/menu/background.png'
import cursor from '../../assets/images/menu/cursor.png'
import bip1 from '../../assets/sounds/walk.ogg'
import bip2 from '../../assets/sounds/piou.ogg'
import bip3 from '../../assets/sounds/noname.ogg'
import save from '../../assets/sounds/save.ogg'

export default class GlobalLoader extends Scene {
  constructor () {
    super('globalLoader')
  }

  preload () {
    this.#loadAssets()
  }

  create () {
    this.scene.start('splashScreen')
    // this.scene.start('mainMenu')
    // this.scene.start('level1Loader')
  }

  #loadAssets () {
    this.load.bitmapFont('atomic', atomicsc, atomicscXML)
    this.load.image('menuBackground', background)
    this.load.image('menuCursor', cursor)
    this.load.audio('bip1', bip1)
    this.load.audio('bip2', bip2)
    this.load.audio('bip3', bip3)
    this.load.audio('save', save)
  }
}
