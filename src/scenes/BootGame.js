import { Scene } from 'phaser'
import ambient2 from '../assets/music/ambient2.ogg'
import { addText, typewriteText } from '../helpers/text'
import { addBackground } from '../helpers/menu'
import { GAME_HEIGHT, GUTTER } from '../constants/sizes'
import { CYAN } from '../constants/colors'
import { tweenLoopWait } from '../helpers/tweens'
import text from '../assets/text/en.json'

export default class BootGame extends Scene {
  #bgImage
  #bgSound
  #continueText

  constructor () {
    super('bootGame')
  }

  preload () {
    this.#loadAssets()
  }

  create () {
    this.#addAssets()
    this.#addTexts()
    this.#start()
    tweenLoopWait(this, this.#continueText, {}, () => this.#bindListeners())
    this.#showAnimation()
  }

  #loadAssets () {
    this.load.audio('ambient2', ambient2)
  }

  #addAssets () {
    this.#bgImage = addBackground(this)

    this.#bgSound = this.sound.add('ambient2', { volume: 0.5 })
  }

  #addTexts () {
    addText(this, text.boot_game.main_title, { level: 'hero', top: 75, tint: CYAN })
    addText(this, text.boot_game.sub_title, { level: 'lg', top: 125 })
    typewriteText(this, text.boot_game.credits, { level: 'sm', top: GAME_HEIGHT / 2 }).setAlpha(0.15)
    this.#continueText = addText(this, text.boot_game.continue, { level: 'sm', top: GAME_HEIGHT - GUTTER }).setAlpha(0)
  }

  #showAnimation () {
    this.tweens.add({
      targets: this.#bgImage,
      duration: 20000,
      repeat: -1,
      yoyo: true,
      scale: {
        getStart: () => 1,
        getEnd: () => 1.5,
      },
    })
  }

  #bindListeners () {
    this.input.keyboard.once('keydown', () => {
      this.cameras.main.fadeOut(2000)
      this.tweens.add({
        targets: this.#bgSound,
        volume: 0,
        duration: 2500,
        onComplete: () => {
          this.#bgSound.stop()
          this.scene.start('intro')
        },
      })
    })
  }

  #start () {
    this.cameras.main.fadeIn(2000)
    this.#bgSound.play()
  }
}
