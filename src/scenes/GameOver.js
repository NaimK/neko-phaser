import Phaser, { Scene } from 'phaser'
import { GAME_WIDTH, GAME_HEIGHT } from '../constants/sizes'
import { getKeys } from '../helpers/user'
import background from '../assets/images/game/menuBackgound3.png'

export default class GameOver extends Scene {
  constructor () {
    super('gameOver')
  }

  preload () {
    this.load.image('background', background)
  }

  create () {
    this.position = [256, 384]
    this.lastPosition = 0

    this.scene.stop('dashBoard')

    this.background = this.add.image(0, 0, 'background')
      .setOrigin(0, 0)
      .setDisplaySize(GAME_WIDTH, GAME_HEIGHT)

    this.title = this.add.bitmapText(GAME_WIDTH / 2, GAME_HEIGHT / 2 - 140, 'atomic', ' Game Over ')
      .setFontSize(70)
      .setOrigin(0.5, 0.5)
      .setTint(0xFF3B00)

    this.retry = this.add.bitmapText(GAME_WIDTH / 4, this.position[0], 'atomic', ' Try again ')
      .setFontSize(48)

    this.quit = this.add.bitmapText(GAME_WIDTH / 4, this.position[1], 'atomic', ' Quit ')
      .setFontSize(48)

    this.head = this.add.image(GAME_WIDTH / 4 - 50, this.position[0], 'head')
      .setOrigin(0, 0)
      .setDisplaySize(50, 50)

    const keysOptions = getKeys()

    this.keys = this.input.keyboard.addKeys({
      up: Phaser.Input.Keyboard.KeyCodes[keysOptions.up],
      down: Phaser.Input.Keyboard.KeyCodes[keysOptions.down],
      fire: Phaser.Input.Keyboard.KeyCodes[keysOptions.fire],
    })

    this.input.keyboard.on('keydown', (event) => {
      if (this.keys.down.isDown && event.key === this.keys.down.originalEvent.key) {
        this.sound.play('bip1')
        this.choose(1)
      } else if (this.keys.up.isDown && event.key === this.keys.up.originalEvent.key) {
        this.sound.play('bip1')
        this.choose(-1)
      } else if (this.keys.fire.isDown && event.key === this.keys.fire.originalEvent.key) {
        this.sound.play('bip2', { volume: 0.1 })
        this.launch()
      }
    })

    // fading the scene from black
    this.cameras.main.fadeIn(1000)
  }

  choose (count) {
    if (this.lastPosition === this.position.length - 1 && count > 0) {
      this.lastPosition = 0
    } else if (this.lastPosition === 0 && count < 0) {
      this.lastPosition = this.position.length - 1
    } else {
      this.lastPosition += count
    }
    this.head.y = this.position[this.lastPosition]
  }

  launch () {
    if (this.lastPosition === 0) {
      this.input.keyboard.enabled = true
      this.scene.start('level1Loader', { loadSavedGame: true })
      this.scene.start('dashBoard')
    }
    if (this.lastPosition === 1) {
      this.scene.start('bootGame')
    }
  }
}
