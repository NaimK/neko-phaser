import GlobalLoader from './loaders/GlobalLoader'
import SplashScreen from './SplashScreen'
import BootGame from './BootGame'
import Intro from './Intro'
import MainMenu from './MainMenu'
import MainMenuOptions from './MainMenuOptions'
import Level1 from './levels/Level1'
import Level1Loader from './loaders/Level1Loader'
import Dashboard from './Dashboard'
import GameOver from './GameOver'
import EndGame from './EndGame'

export default [
  GlobalLoader,
  Level1Loader,
  //
  SplashScreen,
  BootGame,
  Intro,
  MainMenu,
  MainMenuOptions,
  Level1,
  Dashboard,
  GameOver,
  EndGame,
]
