import { Scene } from 'phaser'
import { GAME_HEIGHT, GUTTER } from '../constants/sizes'
import { addBackground } from '../helpers/menu'
import { addText, typewriteText } from '../helpers/text'
import { tweenLoopWait } from '../helpers/tweens'
import text from '../assets/text/en.json'

export default class Intro extends Scene {
  #bgImage
  #continueText

  constructor () {
    super('intro')
  }

  preload () {
    this.#loadAssets()
  }

  create () {
    this.#addAssets()
    this.#addTexts()
    this.#start()
    setTimeout(() => {
      tweenLoopWait(this, this.#continueText, {}, () => this.#bindListeners())
    }, 1000)
  }

  #loadAssets () {}

  #addAssets () {
    this.#bgImage = addBackground(this)
  }

  #addTexts () {
    typewriteText(this, text.intro.text, {
      level: 'sm',
      top: GAME_HEIGHT / 2,
      interval: 100,
      callback: () => this.sound.play('bip1', { volume: 1 }),
    })

    this.#continueText = addText(this, text.intro.continue, {
      level: 'sm',
      top: GAME_HEIGHT - GUTTER,
    })
      .setAlpha(0)
  }

  #bindListeners () {
    this.input.keyboard.once('keydown', () => {
      this.cameras.main.fadeOut(1000)
      setTimeout(() => {
        this.scene.start('mainMenu')
      }, 1000)
    })
  }

  #start () {
    this.cameras.main.fadeIn(1000)
  }
}
