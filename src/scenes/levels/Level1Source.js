import Phaser, { Scene } from 'phaser'
import { GAME_HEIGHT, GAME_WIDTH } from '../../constants/sizes'
import { countTime } from '../../helpers/time'
import { countDeadEnemies } from '../../helpers/stats'

import Player from '../../sprites/Player'
import PowerUp from '../../sprites/PowerUp'
import Crabes from '../../sprites/enemies/Crabes'
import Guepes from '../../sprites/enemies/Guepes'
import Jumpers from '../../sprites/enemies/Jumpers'
import Octopus from '../../sprites/enemies/Octopus'
import RhinoBeetles from '../../sprites/enemies/RhinoBeetles'
import FireBalls from '../../sprites/enemies/FireBalls'
import Boss1 from '../../sprites/enemies/Boss1'
import BossFinal from '../../sprites/enemies/BossFinal'

import Elevators from '../../sprites/environment/Elevators'
import Lava from '../../sprites/environment/Lava'
import WaterFalls from '../../sprites/environment/WaterFalls'
import Doors from '../../sprites/environment/Doors'

export default class Level1Source extends Scene {
  #map
  #tileset
  camPosition = {}

  constructor () {
    super('level1_source')

    this.state = {
      displayPowerUpMsg: false,
    }
  }

  create () {
    this.#prepareMap()
    // this.#setMisc()
    this.#setBackgrounds()
    this.#setLayers()
    this.#setPlayerInWaterEffect()
    this.#setAmbientMusic()
    this.#createPlayerAnimations()
    this.#setPlayerMisc()
    // this.#loadSavedGame()
    this.#setPowerUp()
    this.#createEnemies()
    this.#setFinalBoss()
    // // Environment
    this.#setElevators()
    this.#setLava()
    this.#setWaterfall()
    this.#setDoors()
    this.#setCameras()
    this.#setColliders()
    this.#setMask()
    // this.#loadDashboard()
  }

  #prepareMap () {
    this.#map = this.make.tilemap({ key: 'mapLevel1', tileWidth: 16, tileHeight: 16 })
    this.#tileset = this.#map.addTilesetImage('tileground', 'tiles', 16, 16)
  }

  // TODO: Refactor this
  #setMisc () {
    this.firstTimestamp = new Date().getTime()
    if (!localStorage.getItem('time')) {
      localStorage.setItem('time', 0)
    }

    this.playerFlashTween = null

    this.leaveFullscreenKey = this.input.keyboard
      .addKey('ESC')
      .on('down', () => {
        this.scale.stopFullscreen()
      })
  }

  #setBackgrounds () {
    // parralax backgrounds
    this.para_back = this.add
      .image(0, 0, 'para_back')
      .setDepth(0)
      .setScrollFactor(0.1)
      .setOrigin(0, 0)
      .setDisplaySize(GAME_WIDTH, GAME_HEIGHT)

    this.para_middle = this.add
      .image(0, 0, 'para_middle')
      .setDepth(3)
      .setScrollFactor(0.5)
      .setOrigin(0, 0)
      .setDisplaySize(2048, 1024)

    this.para_lava = this.add
      .image(0, 880, 'bgLava')
      .setDepth(5)
      .setScrollFactor(0.5)
      .setOrigin(0, 0)
      .setDisplaySize(1600, 1200)
  }

  #setLayers () {
    this.backLayer = this.#map
      .createLayer('back', this.tileset, 0, 0)
      .setDepth(4)

    this.middleLayer = this.#map
      .createLayer('middle', this.tileset, 0, 0)
      .setDepth(5)

    this.middleLayer2 = this.#map
      .createLayer('middle2', this.tileset, 0, 0)
      .setDepth(10)

    this.statue = this.#map
      .createLayer('statue', this.tileset, 0, 0)
      .setDepth(98)

    this.eau = this.#map
      .createLayer('eau', this.tileset, 0, 0)
      .setDepth(99)

    this.solLayer = this.#map
      .createLayer('sol', this.tileset, 0, 0)
      .setDepth(100)
    this.frontLayer = this.#map
      .createLayer('front', this.tileset, 0, 0)
      .setDepth(106)
  }

  #setPlayerInWaterEffect () {
    this.waterAmbientMusic = this.sound.add('waterAmbient', { volume: 0.6 })
    this.solLayer.setTileLocationCallback(
      2,
      34,
      26,
      18,
      (e) => {
        if (e === this.player) {
          this.player.onWater = true
          this.player.setDepth(98)
          if (!this.waterAmbientMusic.isPlaying) {
            this.waterAmbientMusic.play()
          }
        }
      },
      this
    )
    this.solLayer.setTileLocationCallback(
      30,
      31,
      1,
      21,
      () => {
        this.player.onWater = false
        this.player.setDepth(105)
        this.waterAmbientMusic.stop()
      },
      this
    )
    this.solLayer.setTileLocationCallback(
      2,
      53,
      29,
      1,
      () => {
        this.player.onWater = false
        this.player.setDepth(105)
        this.waterAmbientMusic.stop()
      },
      this
    )
  }

  #setAmbientMusic () {
    this.ambient1 = this.sound.add('ambient1', { volume: 0.2 })
    this.ambient2 = this.sound.add('ambient2', { volume: 0.1 })
    this.ambient3 = this.sound.add('ambient3', { volume: 0.1 })
  }

  #createPlayerAnimations () {
    this.player = new Player(this, 1924, 158, { key: 'player' })
    // TODO: missing this prop
    this.playerHurt = false
    this.player.body.setSize(15, 35, 6, 11)

    this.anims.create({
      key: 'walkShoot',
      frames: this.anims.generateFrameNumbers('playerShoot', {
        start: 0,
        end: 3,
        first: 0,
      }),
      frameRate: 10,
      repeat: -1,
    })
    this.anims.create({
      key: 'runShoot',
      frames: this.anims.generateFrameNumbers('playerShoot', {
        start: 0,
        end: 3,
        first: 0,
      }),
      frameRate: 5,
      repeat: -1,
    })
    this.anims.create({
      key: 'jumpBoost',
      frames: this.anims.generateFrameNumbers('idle', {
        start: 0,
        end: 4,
        first: 0,
      }),
      frameRate: 4,
      repeat: -1,
    })
    this.anims.create({
      key: 'stand',
      frames: this.anims.generateFrameNumbers('stand', {
        start: 1,
        end: 1,
        first: 1,
      }),
      frameRate: 5,
      repeat: -1,
    })
    this.anims.create({
      key: 'jump',
      frames: this.anims.generateFrameNumbers('jumpVertical', {
        start: 0,
        end: 4,
        first: 0,
      }),
      frameRate: 10,
      repeat: 0,
    })
    this.anims.create({
      key: 'jumpVertical',
      frames: this.anims.generateFrameNumbers('jumpVertical', {
        start: 0,
        end: 4,
        first: 0,
      }),
      frameRate: 10,
      repeat: 0,
    })
    this.anims.create({
      key: 'duck',
      frames: this.anims.generateFrameNumbers('stand', {
        start: 0,
        end: 0,
        first: 0,
      }),
      frameRate: 1,
      repeat: -1,
    })
    this.anims.create({
      key: 'shootup',
      frames: this.anims.generateFrameNumbers('shootUp', {
        start: 0,
        end: 0,
        first: 0,
      }),
      frameRate: 1,
      repeat: -1,
    })
    this.anims.create({
      key: 'morphingBall',
      frames: this.anims.generateFrameNumbers('morphingBall', {
        start: 0,
        end: 3,
        first: 0,
      }),
      frameRate: 16,
      repeat: -1,
    })
    this.anims.create({
      key: 'morphingBallIdle',
      frames: this.anims.generateFrameNumbers('morphingBall', {
        start: 0,
        end: 0,
        first: 0,
      }),
      frameRate: 1,
      repeat: -1,
    })
  }

  #setPlayerMisc () {
    // player walk and run sounds
    this.walkplay = false
    this.walkk = this.sound.add('run', { volume: 0.8 })
    this.player.on('animationupdate', () => {
      if (
        this.player.anims.currentAnim.key === 'runShoot' &&
        !this.walkplay &&
        this.player.body.blocked.down
      ) {
        this.walkplay = true
        this.walkk.play()
        this.time.addEvent({
          delay: 150,
          callback: () => {
            this.walkplay = false
          },
        })
      }
      if (
        this.player.anims.currentAnim.key === 'walkShoot' &&
        !this.walkplay &&
        this.player.body.blocked.down
      ) {
        this.walkplay = true
        this.walkk.play()
        this.time.addEvent({
          delay: 250,
          callback: () => {
            this.walkplay = false
          },
        })
      }
    })

    // player bullets
    this.player.bullets = this.physics.add.group({
      defaultKey: 'bullet',
      maxSize: 10,
      allowGravity: false,
      createIfNull: true,
    })
    this.anims.create({
      key: 'bull',
      frames: this.anims.generateFrameNumbers('bullet', {
        start: 0,
        end: 2,
        first: 0,
      }),
      frameRate: 10,
      repeat: -1,
    })
    this.anims.create({
      key: 'impact',
      frames: this.anims.generateFrameNumbers('impact', {
        start: 0,
        end: 5,
        first: 0,
      }),
      frameRate: 20,
      repeat: 0,
    })

    // player missiles
    this.player.missiles = this.physics.add.group({
      defaultKey: 'missile',
      maxSize: 1,
      allowGravity: false,
      createIfNull: true,
    })
    this.anims.create({
      key: 'missile',
      frames: this.anims.generateFrameNumbers('missile', {
        start: 0,
        end: 2,
        first: 0,
      }),
      frameRate: 10,
      repeat: -1,
    })

    // player swell
    this.player.swells = this.physics.add.group({
      defaultKey: 'swell',
      maxSize: 10,
      allowGravity: false,
      createIfNull: true,
    })
    this.anims.create({
      key: 'swell',
      frames: this.anims.generateFrameNumbers('swell', {
        start: 0,
        end: 7,
        first: 0,
      }),
      frameRate: 24,
      repeat: -1,
    })

    // player morphing bomb
    this.player.bombs = this.physics.add.group({
      defaultKey: 'bomb',
      maxSize: 3,
      allowGravity: false,
      createIfNull: true,
    })
    this.anims.create({
      key: 'bomb',
      frames: this.anims.generateFrameNumbers('bomb', {
        start: 0,
        end: 1,
        first: 0,
      }),
      frameRate: 1,
      repeat: -1,
    })
    this.anims.create({
      key: 'impactBomb',
      frames: this.anims.generateFrameNumbers('impact', {
        start: 0,
        end: 5,
        first: 0,
      }),
      frameRate: 10,
      repeat: 0,
    })

    // player laser
    this.player.lasers = this.physics.add.group({
      defaultKey: 'laser',
      maxSize: 10,
      allowGravity: false,
      createIfNull: true,
    })
  }

  #loadSavedGame () {
    if (this.data.systems.settings.data.loadSavedGame) {
      this.loadGame()
    }
    if (!localStorage.getItem('userSavedGame')) {
      this.transmission(
        'New transmision-A problem occured during-the material transfer on planet-Sorry for inconvenience.'
      )
      this.saveGame()
    }
  }

  #setPowerUp () {
    this.anims.create({
      key: 'powerupYellow',
      frames: this.anims.generateFrameNumbers('powerupYellow', {
        start: 0,
        end: 5,
        first: 0,
      }),
      frameRate: 10,
      yoyo: false,
      repeat: -1,
    })
    this.anims.create({
      key: 'powerupBlue',
      frames: this.anims.generateFrameNumbers('powerupBlue', {
        start: 0,
        end: 5,
        first: 0,
      }),
      frameRate: 10,
      yoyo: false,
      repeat: -1,
    })
    this.anims.create({
      key: 'powerupRed',
      frames: this.anims.generateFrameNumbers('powerupRed', {
        start: 0,
        end: 5,
        first: 0,
      }),
      frameRate: 10,
      yoyo: false,
      repeat: -1,
    })
    this.anims.create({
      key: 'powerupGreen',
      frames: this.anims.generateFrameNumbers('powerupGreen', {
        start: 0,
        end: 5,
        first: 0,
      }),
      frameRate: 10,
      yoyo: false,
      repeat: -1,
    })
    this.anims.create({
      key: 'powerUp',
      frames: this.anims.generateFrameNumbers('powerUp', {
        start: 0,
        end: 6,
        first: 0,
      }),
      frameRate: 10,
      yoyo: true,
      repeat: -1,
    })
    this.powerups = []
    this.#map.objects[0].objects.forEach((element) => {
      if (this.player.inventory.powerUp[element.properties.id] === 0) {
        this[element.name] = new PowerUp(this, element.x, element.y - 16, {
          key: element.properties.key,
          name: element.properties.name,
          ability: element.properties.ability,
          text: element.properties.text,
          id: element.properties.id,
        })
        this[element.name]
          .setDisplayOrigin(0, 0)
          .animate(element.properties.powerup, true)
        this[element.name].body.setSize(16, 16).setAllowGravity(false)
        this.powerups.push(this[element.name])
      }
    })
    this.giveLifeGroup = []
  }

  #createEnemies () {
    this.anims.create({
      key: 'enemyExplode',
      frames: this.anims.generateFrameNumbers('enemyExplode', {
        start: 0,
        end: 5,
        first: 0,
      }),
      frameRate: 15,
      yoyo: false,
      repeat: 0,
    })
    this.anims.create({
      key: 'bossExplode',
      frames: this.anims.generateFrameNumbers('enemyExplode', {
        start: 0,
        end: 5,
        first: 0,
      }),
      frameRate: 15,
      yoyo: false,
      repeat: 10,
    })
    this.explodeSprite = this.add.group({
      defaultKey: 'transparentPixel',
      maxSize: 30,
      allowGravity: false,
      createIfNull: true,
    })
    // anims enemies
    this.anims.create({
      key: 'crabe',
      frames: this.anims.generateFrameNumbers('crabe', {
        start: 0,
        end: 3,
        first: 0,
      }),
      frameRate: 8,
      yoyo: false,
      repeat: -1,
    })
    this.anims.create({
      key: 'guepe',
      frames: this.anims.generateFrameNumbers('guepe', {
        start: 0,
        end: 2,
        first: 0,
      }),
      frameRate: 10,
      yoyo: false,
      repeat: -1,
    })
    this.anims.create({
      key: 'guepe2',
      frames: this.anims.generateFrameNumbers('guepe2', {
        start: 0,
        end: 2,
        first: 0,
      }),
      frameRate: 10,
      yoyo: false,
      repeat: -1,
    })
    this.anims.create({
      key: 'jumper1Idle',
      frames: this.anims.generateFrameNumbers('jumper', {
        start: 0,
        end: 3,
        first: 0,
      }),
      frameRate: 10,
      yoyo: false,
      repeat: -1,
    })
    this.anims.create({
      key: 'jumper1Jump',
      frames: this.anims.generateFrameNumbers('jumper', {
        start: 4,
        end: 4,
        first: 4,
      }),
      frameRate: 10,
      yoyo: false,
      repeat: 0,
    })
    this.anims.create({
      key: 'jumper2Idle',
      frames: this.anims.generateFrameNumbers('jumper2', {
        start: 0,
        end: 3,
        first: 0,
      }),
      frameRate: 10,
      yoyo: false,
      repeat: -1,
    })
    this.anims.create({
      key: 'jumper2Jump',
      frames: this.anims.generateFrameNumbers('jumper2', {
        start: 4,
        end: 4,
        first: 4,
      }),
      frameRate: 10,
      yoyo: false,
      repeat: 0,
    })
    this.anims.create({
      key: 'octopus',
      frames: this.anims.generateFrameNumbers('octopus', {
        start: 0,
        end: 3,
        first: 0,
      }),
      frameRate: 5,
      yoyo: false,
      repeat: -1,
    })
    this.anims.create({
      key: 'octopusIdle',
      frames: this.anims.generateFrameNumbers('octopus', {
        start: 2,
        end: 2,
        first: 2,
      }),
      frameRate: 1,
      yoyo: false,
      repeat: -1,
    })
    this.anims.create({
      key: 'fireball',
      frames: this.anims.generateFrameNumbers('fireball', {
        start: 0,
        end: 2,
        first: 0,
      }),
      frameRate: 5,
      yoyo: false,
      repeat: -1,
    })
    this.anims.create({
      key: 'rhinoWalk',
      frames: this.anims.generateFrameNumbers('rhinobeetle', {
        start: 0,
        end: 2,
        first: 0,
      }),
      frameRate: 5,
      yoyo: false,
      repeat: -1,
    })
    this.anims.create({
      key: 'rhinoBall',
      frames: this.anims.generateFrameNumbers('rhinobeetle', {
        start: 3,
        end: 6,
        first: 3,
      }),
      frameRate: 5,
      yoyo: false,
      repeat: -1,
    })
    this.enemyGroup = []
    // the crabs
    this.#map.objects[1].objects.forEach((element) => {
      this[element.name] = new Crabes(this, element.x, element.y - 16, {
        key: element.properties.key,
        life: element.properties.life,
        damage: element.properties.damage,
      })
      this[element.name].animate(element.properties.key, true)
      this.enemyGroup.push(this[element.name])
    })
    // the wasps
    this.#map.objects[3].objects.forEach((element) => {
      this[element.name] = new Guepes(this, element.x, element.y - 16, {
        key: element.properties.key,
        life: element.properties.life,
        damage: element.properties.damage,
      })
      this[element.name].animate(element.properties.key, true)
      this.enemyGroup.push(this[element.name])
    })
    // the jumpers
    this.#map.objects[4].objects.forEach((element) => {
      this[element.name] = new Jumpers(this, element.x, element.y - 16, {
        key: element.properties.key,
        life: element.properties.life,
        damage: element.properties.damage,
      })
      this.enemyGroup.push(this[element.name])
    })
    // the octopus
    this.#map.objects[9].objects.forEach((element) => {
      this[element.name] = new Octopus(this, element.x, element.y - 16, {
        key: element.properties.key,
        life: element.properties.life,
        damage: element.properties.damage,
      })
      this.enemyGroup.push(this[element.name])
    })
    // the rhinobeetles
    this.loadRhino = false
    if (!this.player.inventory.rhino) {
      this.solLayer.setTileLocationCallback(
        53,
        103,
        1,
        8,
        (e) => {
          if (e === this.player && !this.loadRhino) {
            this.loadRhino = true
            this.#map.objects[11].objects.forEach((element) => {
              this[element.name] = new RhinoBeetles(
                this,
                element.x,
                element.y - 16,
                {
                  key: element.properties.key,
                  life: element.properties.life,
                  damage: element.properties.damage,
                }
              )
              this.enemyGroup.push(this[element.name])
            })
          }
        },
        this
      )
    }
  }

  #setFinalBoss () {
    this.anims.create({
      key: 'bossFinalAttack',
      frames: this.anims.generateFrameNumbers('bossFinal', {
        start: 0,
        end: 7,
        first: 0,
      }),
      frameRate: 16,
      yoyo: false,
      repeat: -1,
    })
    this.anims.create({
      key: 'bossFinalIntro',
      frames: this.anims.generateFrameNumbers('bossFinal', {
        start: 8,
        end: 8,
        first: 8,
      }),
      frameRate: 1,
      yoyo: false,
      repeat: -1,
    })
    this.bossFinalReady = false
    this.bossFinalstarted = false
    this.bossFinalBattlePreventSave = false
    if (!this.player.inventory.bossFinal) {
      this.solLayer.setTileLocationCallback(
        70,
        127,
        8,
        1,
        (e) => {
          if (
            !this.bossFinalReady &&
              e === this.player &&
              !this.player.inventory.bossFinal
          ) {
            this.bossFinalBattlePrep()
          }
        },
        this
      )
      this.solLayer.setTileLocationCallback(
        1,
        188,
        116,
        1,
        (e) => {
          if (
            !this.bossFinalReady &&
              e === this.player &&
              !this.player.inventory.bossFinal
          ) {
            this.bossFinalBattlePrep()
          }
        },
        this
      )
    }
  }

  #setElevators () {
    this.elevatorGroup = []
    this.#map.objects[2].objects.forEach((element) => {
      this[element.name] = new Elevators(this, element.x + 24, element.y, {
        key: element.properties.key,
        up: element.properties.up,
        down: element.properties.down,
        position: element.properties.position,
      })
      this.elevatorGroup.push(this[element.name])
    })
  }

  #setLava () {
    this.anims.create({
      key: 'lava',
      frames: this.anims.generateFrameNumbers('lava', {
        start: 0,
        end: 2,
        first: 0,
      }),
      frameRate: 2,
      yoyo: false,
      repeat: -1,
    })
    this.lavaGroup = []
    this.#map.objects[7].objects.forEach((element) => {
      this[element.name] = new Lava(this, element.x, element.y, {
        key: element.properties.key,
      })
      this[element.name].animate(element.properties.key, true)
      this[element.name].setDepth(11)
      this.lavaGroup.push(this[element.name])
    })
    // lava fall, same group as lava
    this.anims.create({
      key: 'lavaFall',
      frames: this.anims.generateFrameNumbers('lavaFall', {
        start: 0,
        end: 2,
        first: 0,
      }),
      frameRate: 3,
      yoyo: false,
      repeat: -1,
    })
    this.#map.objects[6].objects.forEach((element) => {
      this[element.name] = new Lava(this, element.x + 16, element.y - 8, {
        key: element.properties.key,
      })
      this[element.name].setDisplaySize(32, 32).setDepth(10)
      this[element.name].animate(element.properties.key, true)
      this.lavaGroup.push(this[element.name])
    })
    // fireballs, same group as lava
    this.#map.objects[10].objects.forEach((element) => {
      this[element.name] = new FireBalls(this, element.x + 16, element.y - 8, {
        key: element.properties.key,
      })
      this[element.name].animate(element.properties.key, true)
      this.lavaGroup.push(this[element.name])
    })

    // LAVA RISE
    this.lavaRiseFlag = false
    this.onSismic = false
    this.isTheEnd = false // My only friend, the end
  }

  #setWaterfall () {
    this.anims.create({
      key: 'waterFall',
      frames: this.anims.generateFrameNumbers('waterFall', {
        start: 0,
        end: 2,
        first: 0,
      }),
      frameRate: 6,
      yoyo: false,
      repeat: -1,
    })
    this.#map.objects[5].objects.forEach((element) => {
      this[element.name] = new WaterFalls(this, element.x + 8, element.y - 8, {
        key: element.properties.key,
      })
      this[element.name].animate(element.properties.key, true)
    })
  }

  #setDoors () {
    this.doorGroup = []
    this.#map.objects[8].objects.forEach((element, i) => {
      if (element.properties.side === 'right') {
        this[element.name] = new Doors(this, element.x + 3, element.y + 9, {
          key: element.properties.key,
          side: element.properties.side,
        })
        this[element.name].body.setSize(10, 47)
      }
      if (element.properties.side === 'left') {
        this[element.name] = new Doors(this, element.x + 16, element.y + 9, {
          key: element.properties.key,
          side: element.properties.side,
        })
        this[element.name].flipX = true
        this[element.name].body.setSize(10, 47)
      }
      this.doorGroup.push(this[element.name])
      if (i === 7) {
        this[element.name].body.enable = false
        this[element.name].alpha = 0
      }
    })
  }

  #setCameras () {
    // set bounds so the camera won't go outside the game world
    this.cameras.main.setBounds(
      0,
      0,
      this.#map.widthInPixels,
      this.#map.heightInPixels
    )
    // make the camera follow the player
    this.cameras.main.startFollow(this.player, true, 0.4, 0.1)
    this.cameras.main.transparent = true
    this.cameras.main.setZoom(2)
    this.cameras.main.fadeIn(200)
  }

  #setColliders () {
    this.solLayer.setCollisionByProperty({ collides: true })

    this.physics.add.collider(this.player, this.solLayer, null)
    this.physics.add.collider(this.doorGroup, this.player, null)
    this.physics.add.collider(this.enemyGroup, this.solLayer, null)
    this.physics.add.collider(this.enemyGroup, this.doorGroup, null)
    this.physics.add.collider(this.lavaGroup, this.solLayer, null)
    this.physics.add.collider(
      [this.player.bullets, this.player.swells],
      this.solLayer,
      this.player.bulletKill,
      null,
      this.player
    )
    this.physics.add.collider(
      this.player.missiles,
      this.solLayer,
      this.player.missileKill,
      null,
      this.player
    )
    this.physics.add.collider(
      this.player.lasers,
      this.solLayer,
      this.player.laserKill,
      null,
      this.player
    )
    this.physics.add.collider(
      [this.player.bullets, this.player.swells],
      this.doorGroup,
      (bull, d) => this.player.bulletKill(d),
      null,
      this.player.bullets
    )
    this.physics.add.collider(
      this.player.lasers,
      this.doorGroup,
      (bull, d) => this.player.laserKill(d),
      null,
      this.player.lasers
    )
    this.physics.add.collider(
      this.player.missiles,
      this.doorGroup,
      (d, miss) => this.openDoor(d, miss),
      null,
      this
    )
    this.physics.add.collider(
      this.elevatorGroup,
      [this.player, this.player.bullets],
      (elm) => elm.handleElevator(this.player),
      null,
      this
    )
    this.physics.add.overlap(
      this.lavaGroup,
      this.player,
      () => this.player.handleLava(),
      null,
      this.player
    )
    this.physics.add.overlap(
      this.giveLifeGroup,
      this.player,
      (elm) => this.player.getLife(elm),
      null,
      this.player
    )
    this.physics.add.overlap(
      this.powerups,
      this.player,
      (elm) => this.getPowerUp(elm),
      null,
      this
    )
    this.physics.add.overlap(
      this.enemyGroup,
      this.player,
      (elm) => this.playerIsHit(elm),
      null,
      this
    )
    this.physics.add.overlap(
      [
        this.player.bullets,
        this.player.swells,
        this.player.missiles,
        this.player.lasers,
      ],
      this.enemyGroup,
      (elm, bull) => this.enemyIsHit(bull, elm, this.player),
      null,
      this.player
    )
  }

  #setMask () {
    this.mask = this.make
      .graphics({ fillStyle: { color: 0xffffff }, add: false })
      .fillCircleShape(new Phaser.Geom.Circle(0, 6, 30))

    this.frontLayer.mask = new Phaser.Display.Masks.BitmapMask(this, this.mask)
    this.frontLayer.mask.invertAlpha = true
  }

  #loadDashboard () {
    this.events.emit('loadingDone')
  }

  // ====================================================================

  // Original methods

  // ====================================================================

  // ok
  update () {
    this.camPosition = this.cameras.main.midPoint
    this.playMusic()
    // lava rise
    if (this.lavaRise) {
      this.stopLavaRise()
      this.sismicActivity()
    }
    // player sonar
    if (
      this.player.state.onMorphingBall &&
      this.player.inventory.morphingSonar
    ) {
      this.mask.x = this.player.x
      this.mask.y = this.player.y
    } else {
      this.mask.x = -300
      this.mask.y = -300
    }

    if (this.state.displayPowerUpMsg) {
      this.msgtext.x = this.player.x
      this.msgtext.y = this.player.y - 60
    }
    if (this.modalText) {
      this.modalText.x = this.player.x
      this.modalText.y = this.player.y - 100
    }
  }

  // ok
  playMusic () {
    if (this.player.y <= 1024 && !this.isTheEnd) {
      if (this.ambient2.isPlaying) {
        this.ambient2.stop()
      }
      if (!this.ambient1.isPlaying) {
        this.ambient1.play()
      }
    }
    if (this.player.y <= 2080 && this.player.y > 1024) {
      if (this.ambient1.isPlaying || this.ambient3.isPlaying) {
        this.ambient1.stop()
        this.ambient3.stop()
      }
      if (!this.ambient2.isPlaying) {
        this.ambient2.play()
      }
    }
    if (this.player.y <= 3056 && this.player.y > 2080) {
      if (this.ambient2.isPlaying) {
        this.ambient2.stop()
      }
      if (!this.ambient3.isPlaying && !this.bossMusic.isPlaying) {
        this.ambient3.play()
      }
    }
  }

  // ok
  sismicActivity () {
    if (!this.onSismic) {
      this.onSismic = true
      const rdm = Phaser.Math.Between(2000, 5000)
      this.shakeCamera(1000)
      this.sound.play('shake', { volume: 0.5 })
      // this.sound.play('shake2', { volume: 0.5 });
      this.time.addEvent({
        delay: rdm,
        callback: () => {
          this.onSismic = false
        },
      })
    }
  }

  // ok
  startLavaRise () {
    this.transmission(
      'ALERT-High sismic activity detected-return to spacehip for evacuation'
    )
    this.solLayer.setTileLocationCallback(
      119,
      3,
      1,
      9,
      (e) => {
        if (e === this.player) {
          this.endMission()
        }
      },
      this
    )
    this.time.addEvent({
      delay: 5000,
      callback: () => {
        this.lavaRise = this.physics.add
          .image(0, 3072, 'lavaPixel')
          .setOrigin(0, 0)
          .setDisplaySize(2048, 3072)
          .setDepth(99)
          .setAlpha(0.9)
          .setOffset(0, 0)
        this.lavaRise.body
          .setVelocityY(-43)
          .setImmovable(true)
          .setAllowGravity(false)
        this.physics.add.overlap(
          this.player,
          this.lavaRise,
          () => this.player.handleLava(),
          null,
          this.player
        )
      },
    })
  }

  endMission () {
    if (!this.isTheEnd) {
      this.isTheEnd = true
      this.round = this.add
        .sprite(this.player.x, this.player.y, 'whitePixel')
        .setOrigin(0.5, 0.5)
        .setDepth(1000)
        .setDisplaySize(4096, 4096)
        .setAlpha(0)
      this.countTime()
      this.ambient1.stop()
      this.tween = this.tweens.add({
        targets: this.round,
        ease: 'Sine.easeInOut',
        duration: 1500,
        delay: 0,
        repeat: 0,
        yoyo: false,
        alpha: {
          getStart: () => 0,
          getEnd: () => 1,
        },
        onComplete: () => {
          this.lavaRise = null
          this.ambient1.stop()
          this.scene.start('endGame')
        },
      })
    }
  }

  // ok
  stopLavaRise () {
    if (!this.lavaRiseFlag && this.lavaRise.y > 0) {
      this.lavaRiseFlag = true
    }
    if (this.lavaRise.y < 0) {
      this.lavaRise.setVelocityY(0)
    }
  }

  // ok
  getPowerUp (elm) {
    this.state.displayPowerUpMsg = true
    if (elm.state.ability === 'energy') {
      this.player.addEnergy()
      this.events.emit('setHealth', { life: this.player.inventory.life })
    } else if (elm.state.ability === 'speedfire') {
      this.player.addSpeedFire()
    } else if (
      elm.state.ability === 'missile' &&
      !this.player.inventory.boss1
    ) {
      this.state.displayPowerUpMsg = false
      return
    } else if (elm.state.ability === 'missile' && this.player.inventory.boss1) {
      this.player.addMissile()
    } else if (elm.state.ability === 'laser') {
      this.player.inventory[elm.state.ability] = true
      this.player.addLaser()
    } else if (elm.state.ability === 'swell') {
      this.player.inventory[elm.state.ability] = true
      this.player.addSwell()
    } else {
      this.player.inventory[elm.state.ability] = true
    }
    this.sound.play('powerUp')
    this.player.inventory.powerUp[elm.state.id] = 1

    this.msgtext = this.add
      .bitmapText(0, 0, 'atomic', elm.state.text, 12, 1)
      .setOrigin(0.5, 0.5)
      .setAlpha(1)
      .setDepth(210)
    elm.destroy()

    this.fadingTween = this.tweens.add({
      targets: [this.msgtext],
      ease: 'Sine.easeInOut',
      duration: 2000,
      delay: 3000,
      repeat: 0,
      yoyo: false,
      alpha: {
        getStart: () => 1,
        getEnd: () => 0,
      },
      onComplete: () => {
        this.state.displayPowerUpMsg = false
      },
    })
  }

  // ====================================================================
  pauseGame () {
    if (!this.player.state.pause) {
      this.countTime()
      this.player.state.pause = true
      this.physics.pause()
      this.player.anims.pause(this.player.anims.currentFrame)
      this.msg = this.add
        .image(
          this.cameras.main.worldView.x,
          this.cameras.main.worldView.y,
          'blackPixel'
        )
        .setOrigin(0, 0)
        .setDisplaySize(400, 256)
        .setAlpha(0.9)
        .setDepth(109)

      this.msgText = this.add
        .bitmapText(
          this.cameras.main.worldView.x + 200,
          this.cameras.main.worldView.y + 128,
          'atomic',
          'PAUSE',
          50,
          1
        )
        .setOrigin(0.5, 0.5)
        .setAlpha(1)
        .setDepth(110)

      // save part
      this.position = [
        this.cameras.main.worldView.y + 180,
        this.cameras.main.worldView.y + 200,
      ]
      this.lastPosition = 0

      this.continueBtn = this.add
        .bitmapText(
          this.cameras.main.worldView.x + GAME_WIDTH / 4,
          this.position[0],
          'atomic',
          ' Continue ',
          16,
          1
        )
        .setTint(0xff3b00)
        .setDepth(110)
        .setOrigin(0.5, 0.5)

      this.saveGameBtn = this.add
        .bitmapText(
          this.cameras.main.worldView.x + GAME_WIDTH / 4,
          this.position[1],
          'atomic',
          '      Save ',
          16,
          1
        )
        .setTint(0xff3b00)
        .setDepth(110)
        .setOrigin(0.85, 0.5)

      this.head = this.add
        .image(
          this.cameras.main.worldView.x + GAME_WIDTH / 4 - 60,
          this.position[0],
          'head'
        )
        .setOrigin(0.5, 0.65)
        .setDisplaySize(15, 15)
        .setAlpha(1)
        .setDepth(110)
    }
  }

  // ====================================================================
  choose () {
    this.player.chooseDone = true
    if (this.player.state.pause) {
      if (this.lastPosition === 1) {
        this.lastPosition = 0
      } else {
        this.lastPosition += 1
      }
      this.head.y = this.position[this.lastPosition]
      this.time.addEvent({
        delay: 300,
        callback: () => {
          this.player.chooseDone = false
        },
      })
    }
  }

  // ====================================================================
  launch () {
    this.player.chooseDone = true
    if (this.player.state.pause) {
      if (this.lastPosition === 0) {
        this.player.state.pause = false
        this.scene.scene.physics.resume()
        this.player.anims.resume(this.player.anims.currentFrame)
        this.msgText.destroy()
        this.msg.destroy()
        this.continueBtn.destroy()
        this.saveGameBtn.destroy()
        this.head.destroy()
        this.firstTimestamp = new Date().getTime()
        this.time.addEvent({
          delay: 300,
          callback: () => {
            this.player.chooseDone = false
          },
        })
      }
      if (this.lastPosition === 1) {
        this.saveGame()
        this.time.addEvent({
          delay: 300,
          callback: () => {
            this.player.chooseDone = false
          },
        })
      }
    }
  }

  // Ok
  // ====================================================================
  shakeCamera (e) {
    this.cameras.main.shake(e, 0.005)
  }

  // Ok
  // ====================================================================
  flashCamera () {
    this.cameras.main.flash(1000)
  }

  // Ok
  // ====================================================================
  playerIsHit (elm) {
    if (!this.playerHurt) {
      this.playerHurt = true // flag
      // this.player.animate('hurt');
      this.sound.play('playerHit')
      this.player.inventory.life -= elm.state.damage
      this.playerFlashTween = this.tweens.add({
        targets: this.player,
        ease: 'Sine.easeInOut',
        duration: 200,
        delay: 0,
        repeat: 5,
        yoyo: true,
        alpha: {
          getStart: () => 0,
          getEnd: () => 1,
        },
        onComplete: () => {
          this.player.alpha = 1
          this.playerHurt = false
          // this.player.animate('stand');
        },
      })
      // if player is dead, launch deadth sequence
      if (this.player.inventory.life <= 0) {
        this.player.state.dead = true
        this.playerDead = true
        this.physics.pause()
        this.input.enabled = false
        this.player.anims.pause(this.player.anims.currentFrame)
        this.playerFlashTween.stop()
        this.player.inventory.life = 0
        this.player.setTintFill(0xffffff)
        this.player.setDepth(2000)

        this.round = this.add.sprite(
          this.player.x,
          this.player.y,
          'whitePixel'
        )
        this.round.setOrigin(0.5, 0.5)
        this.round.setDepth(1000)
        this.round.displayWidth = 2
        this.round.displayHeight = 2
        this.sound.play('playerDead', { volume: 0.2 })

        this.tween = this.tweens.add({
          targets: this.round,
          ease: 'Sine.easeInOut',
          scaleX: 1,
          scaleY: 1,
          duration: 2500,
          delay: 800,
          onComplete: () => {
            this.input.enabled = true
            this.playerIsDead()
          },
        })
      }
    }
    this.events.emit('setHealth', { life: this.player.inventory.life }) // set health dashboard scene
  }

  // Ok
  // ====================================================================
  playerIsDead () {
    let d = localStorage.getItem('d')
    d = JSON.parse(d)
    d += 1
    localStorage.setItem('d', d)
    this.bossMusic.stop()
    this.ambient1.stop()
    this.ambient2.stop()
    this.ambient3.stop()
    this.waterAmbientMusic.stop()
    this.countTime()
    if (this.lavaRise) {
      this.lavaRise = null
    }
    this.player.state.dead = false
    this.scene.start('gameOver')
  }

  countTime () {
    this.firstTimestamp = countTime(this.firstTimestamp)
  }

  // Ok
  // ====================================================================
  enemyIsHit (bull, elm) {
    const el = elm
    if (!el.getFired) {
      el.getFired = true
      if (
        this.player.state.selectedWeapon === 'missile' ||
        this.player.state.selectedWeapon === 'bullet' ||
        this.player.state.selectedWeapon === 'swell'
      ) {
        this.player[`${this.player.state.selectedWeapon}Kill`](bull)
      }
      if (el === this.rhino1 || el === this.rhino2 || el === this.rhino3) {
        if (el.vulnerable) {
          el.looseLife(
            this.player.inventory[`${this.player.state.selectedWeapon}Damage`]
          )
          el.setTintFill(0xdddddd)
          this.time.addEvent({
            delay: 50,
            callback: () => {
              el.clearTint()
            },
          })
        }
      } else {
        el.looseLife(
          this.player.inventory[`${this.player.state.selectedWeapon}Damage`]
        )
        el.setTintFill(0xdddddd)
        this.time.addEvent({
          delay: 50,
          callback: () => {
            el.clearTint()
          },
        })
      }
      this.hitTimer = this.time.addEvent({
        delay: 120,
        callback: () => {
          el.getFired = false
        },
      })
    }
    if (el.state.life < 0) {
      el.clearTint()
      // kill the enemy
      if (el === this.rhino1 || el === this.rhino2 || el === this.rhino3) {
        this.giveLife = this.physics.add.staticSprite(el.x, el.y, 'powerUp')
        this.giveLife.setDepth(105)
        this.giveLife.health = el.state.giveLife
        this.giveLife.body.setSize(23, 21)
        this.giveLife.anims.play('powerUp')
        this.giveLifeGroup.push(this.giveLife)
        countDeadEnemies()
        this.player.state.rhinoCount += 1
        if (this.player.state.rhinoCount === 3) {
          this.player.inventory.rhino = true
        }
      }
      if (el === this.boss1) {
        this.giveLife = this.physics.add.staticSprite(el.x, el.y, 'powerUp')
        this.giveLife.setDepth(105)
        this.giveLife.health = el.state.giveLife
        this.giveLife.body.setSize(23, 21)
        this.giveLife.anims.play('powerUp')
        this.giveLifeGroup.push(this.giveLife)
        this.boss1.setTintFill(0xdddddd)
        this.missile.body.reset(this.boss1.x, this.boss1.y)
        this.bossExplode(this.boss1.x, this.boss1.y)
        this.shakeCamera(5000)
        this.time.addEvent({
          delay: Phaser.Math.Between(500, 1200),
          callback: () => {
            this.bossExplode(
              this.boss1.x - 50,
              Phaser.Math.Between(this.boss1.y - 50, this.boss1.y + 50)
            )
            this.bossExplode(
              this.boss1.x - 20,
              Phaser.Math.Between(this.boss1.y - 50, this.boss1.y + 50)
            )
          },
        })
        this.time.addEvent({
          delay: Phaser.Math.Between(500, 1200),
          callback: () => {
            this.bossExplode(
              this.boss1.x,
              Phaser.Math.Between(this.boss1.y - 50, this.boss1.y + 50)
            )
            this.bossExplode(
              this.boss1.x + 20,
              Phaser.Math.Between(this.boss1.y - 50, this.boss1.y + 50)
            )
          },
        })
        this.time.addEvent({
          delay: Phaser.Math.Between(500, 1200),
          callback: () => {
            this.bossExplode(
              this.boss1.x + 50,
              Phaser.Math.Between(this.boss1.y - 50, this.boss1.y + 50)
            )
            this.bossExplode(
              this.boss1.x - 50,
              Phaser.Math.Between(this.boss1.y - 50, this.boss1.y + 50)
            )
          },
        })
        this.time.addEvent({
          delay: Phaser.Math.Between(500, 1200),
          callback: () => {
            this.bossExplode(
              this.boss1.x,
              Phaser.Math.Between(this.boss1.y - 50, this.boss1.y + 50)
            )
            this.bossExplode(
              this.boss1.x,
              Phaser.Math.Between(this.boss1.y - 50, this.boss1.y + 50)
            )
          },
        })
        this.time.addEvent({
          delay: Phaser.Math.Between(500, 1200),
          callback: () => {
            this.bossExplode(
              this.boss1.x + 20,
              Phaser.Math.Between(this.boss1.y - 50, this.boss1.y + 50)
            )
            this.bossExplode(
              this.boss1.x + 50,
              Phaser.Math.Between(this.boss1.y - 50, this.boss1.y + 50)
            )
          },
        })
        countDeadEnemies()
        this.boss1.anims.pause(this.boss1.anims.currentFrame)
        this.boss1.body.setEnable(false)
        this.boss1.setDepth(0)
        this.boss1BattlePreventSave = false
        this.time.addEvent({
          delay: 600,
          callback: () => {
            this.tween = this.tweens.add({
              targets: this.boss1,
              ease: 'Sine.easeInOut',
              duration: 1000,
              delay: 0,
              repeat: 0,
              yoyo: false,
              alpha: {
                getStart: () => 1,
                getEnd: () => 0,
              },
              onComplete: () => {
                this.missile.alpha = 1
                this.player.inventory.boss1 = true
                this.boss1.destroy()
              },
            })
          },
        })
      } else if (el === this.bossFinal) {
        if (!this.bossFinal.isDead) {
          this.bossFinal.isDead = true
          this.bossFinal.body.setVelocityY(500)
          this.bossFinal.setTintFill(0xdddddd)
          this.bossExplode(this.bossFinal.x, this.bossFinal.y)
          this.shakeCamera(5000)
          this.time.addEvent({
            delay: Phaser.Math.Between(500, 1200),
            callback: () => {
              this.bossExplode(
                this.bossFinal.x - 50,
                Phaser.Math.Between(
                  this.bossFinal.y - 50,
                  this.bossFinal.y + 50
                )
              )
              this.bossExplode(
                this.bossFinal.x - 20,
                Phaser.Math.Between(
                  this.bossFinal.y - 50,
                  this.bossFinal.y + 50
                )
              )
            },
          })
          this.time.addEvent({
            delay: Phaser.Math.Between(500, 1200),
            callback: () => {
              this.bossExplode(
                this.bossFinal.x,
                Phaser.Math.Between(
                  this.bossFinal.y - 50,
                  this.bossFinal.y + 50
                )
              )
              this.bossExplode(
                this.bossFinal.x + 20,
                Phaser.Math.Between(
                  this.bossFinal.y - 50,
                  this.bossFinal.y + 50
                )
              )
            },
          })
          this.time.addEvent({
            delay: Phaser.Math.Between(500, 1200),
            callback: () => {
              this.bossExplode(
                this.bossFinal.x + 50,
                Phaser.Math.Between(
                  this.bossFinal.y - 50,
                  this.bossFinal.y + 50
                )
              )
              this.bossExplode(
                this.bossFinal.x - 50,
                Phaser.Math.Between(
                  this.bossFinal.y - 50,
                  this.bossFinal.y + 50
                )
              )
            },
          })
          this.time.addEvent({
            delay: Phaser.Math.Between(500, 1200),
            callback: () => {
              this.bossExplode(
                this.bossFinal.x,
                Phaser.Math.Between(
                  this.bossFinal.y - 50,
                  this.bossFinal.y + 50
                )
              )
              this.bossExplode(
                this.bossFinal.x,
                Phaser.Math.Between(
                  this.bossFinal.y - 50,
                  this.bossFinal.y + 50
                )
              )
            },
          })
          this.time.addEvent({
            delay: Phaser.Math.Between(500, 1200),
            callback: () => {
              this.bossExplode(
                this.bossFinal.x + 20,
                Phaser.Math.Between(
                  this.bossFinal.y - 50,
                  this.bossFinal.y + 50
                )
              )
              this.bossExplode(
                this.bossFinal.x + 50,
                Phaser.Math.Between(
                  this.bossFinal.y - 50,
                  this.bossFinal.y + 50
                )
              )
            },
          })
          this.bossFinal.anims.pause(this.bossFinal.anims.currentFrame)
          this.bossFinal.setDepth(0)
          this.bossFinal.playRoar('cri4')
          this.bossFinal.body.velocity.y = 500
          this.time.addEvent({
            delay: 600,
            callback: () => {
              this.tween = this.tweens.add({
                targets: this.bossFinal,
                ease: 'Sine.easeInOut',
                duration: 1000,
                delay: 0,
                repeat: 0,
                yoyo: false,
                alpha: {
                  getStart: () => 1,
                  getEnd: () => 0,
                },
                onComplete: () => {
                  countDeadEnemies()
                  this.giveLife = this.physics.add.staticSprite(
                    this.bossFinal.x,
                    this.bossFinal.y,
                    'powerUp'
                  )
                  this.giveLife.setDepth(105)
                  this.giveLife.health = el.state.giveLife
                  this.giveLife.body.setSize(23, 21)
                  this.giveLife.anims.play('powerUp')
                  this.giveLifeGroup.push(this.giveLife)
                  const filteringOptions = {
                    // isNotEmpty: false,
                    isColliding: false,
                    // hasInterestingFace: false
                  }
                  const bossFinalTiles = this.solLayer.getTilesWithinWorldXY(
                    this.bossFinal.x,
                    this.bossFinal.y + 48,
                    48,
                    16,
                    filteringOptions
                  )
                  bossFinalTiles.forEach((e) => {
                    if (e.properties.boss) {
                      this.solLayer.removeTileAt(e.x, e.y, true, true)
                    }
                  })
                  this.enemyExplode(117 * 16, 186 * 16)
                  this.solLayer.removeTileAt(117, 186, true, true)
                  this.enemyExplode(117 * 16, 187 * 16)
                  this.solLayer.removeTileAt(117, 187, true, true)
                  this.enemyExplode(117 * 16, 188 * 16)
                  this.solLayer.removeTileAt(117, 188, true, true)
                  this.enemyExplode(118 * 16, 186 * 16)
                  this.solLayer.removeTileAt(118, 186, true, true)
                  this.enemyExplode(118 * 16, 187 * 16)
                  this.solLayer.removeTileAt(118, 187, true, true)
                  this.enemyExplode(118 * 16, 188 * 16)
                  this.solLayer.removeTileAt(118, 188, true, true)
                  this.startLavaRise()
                  this.player.inventory.bossFinal = true
                  this.bossFinal.destroy()
                },
              })
            },
          })
        }
      } else {
        this.giveLife = this.physics.add.staticSprite(el.x, el.y, 'powerUp')
        this.giveLife.setDepth(105)
        this.giveLife.health = el.state.giveLife
        this.giveLife.body.setSize(23, 21)
        this.giveLife.anims.play('powerUp')
        this.giveLifeGroup.push(this.giveLife)
        this.enemyExplode(el.x, el.y)
        this.enemyDestroy(el)
      }
    }
  }

  // Ok
  enemyDestroy (e) {
    e.destroy()
    countDeadEnemies()
  }

  // Ok
  bossExplode (x, y) {
    this.bossMusic.stop()
    const exp = this.explodeSprite
      .getFirstDead(true, x, y, 'enemyExplode', null, true)
      .setDepth(107)
    this.sound.play('explo2', { volume: 0.3 })
    if (exp) {
      exp.anims
        .play('bossExplode')
        .on('animationrepeat', () => {
          this.sound.play('explo2', { volume: 0.3 })
        })
        .on('animationcomplete', () => {
          exp.destroy()
        })
    }
  }

  // Ok
  enemyExplode (x, y) {
    const exp = this.explodeSprite
      .getFirstDead(true, x, y, 'enemyExplode', null, true)
      .setDepth(107)
    exp.anims.play('enemyExplode').on('animationcomplete', () => {
      exp.destroy()
    })
  }

  // Ok
  // ====================================================================
  openDoor (d, miss) {
    this.player.missileKill(miss)
    d.destroyDoor()
  }

  // Ok
  boss1BattlePrep () {
    this.boss1started = true
    this.solLayer.setTileLocationCallback(78, 77, 1, 3, null)
    this.boss1wallfront = this.add
      .image(1912, 1340, 'boss1wallfront')
      .setDepth(2000)
      .setVisible(true)

    this.boss1 = new Boss1(this, 1930, 1350, { key: 'boss1run' })
    this.boss1.animate('boss1run')
    this.boss1.setAlpha(0)
    this.enemyGroup.push(this.boss1)
    this.boss1.body.setVelocity(0, 0).setEnable(false)
  }

  // Ok
  boss1Battle () {
    if (!this.boss1started) {
      this.boss1BattlePrep()
    }
    this.boss1BattlePreventSave = true
    this.solLayer.setTileLocationCallback(109, 86, 3, 3, null)
    this.boss1.body.setEnable()
    this.physics.add.collider(this.boss1, this.solLayer, null)
    this.boss1.setAlpha(1)
    this.boss1.animate('boss1run')
    this.boss1.intro = true
    this.door7.body.enable = true
    this.door7.setAlpha(1)
  }

  // Ok
  bossFinalBattlePrep () {
    this.bossFinalReady = true
    this.bossFinal = new BossFinal(this, 1250, 2960, { key: 'bossFinal' })
    this.bossFinal.animate('bossFinalIntro')
    this.physics.add.collider(this.bossFinal, this.solLayer, null)
    this.physics.add.overlap(
      this.bossFinal,
      this.player,
      (elm) => this.playerIsHit(elm),
      null,
      this
    )
    this.physics.add.overlap(
      [
        this.player.bullets,
        this.player.swells,
        this.player.missiles,
        this.player.lasers,
      ],
      this.bossFinal,
      (elm, bull) => this.enemyIsHit(bull, elm, this.player),
      null,
      this.player
    )
    this.bossFinal.flames = this.physics.add.group({
      defaultKey: 'fireball',
      maxSize: 900,
      allowGravity: false,
      createIfNull: true,
    })
    this.physics.add.collider(
      this.bossFinal.flames,
      this.solLayer,
      (elm) => {
        this.bossFinal.stopFlame(elm)
      },
      null
    )
    this.physics.add.overlap(
      this.bossFinal.flames,
      this.player,
      () => this.player.handleLava(),
      null,
      this.player
    )
    this.boss1dead = this.add.image(925, 3010, 'boss1dead').setDepth(100)
    this.boss1dead.flipX = true
    this.solLayer.setTileLocationCallback(
      59,
      188,
      1,
      6,
      (e) => {
        if (
          e === this.player &&
          !this.player.inventory.bossFinal &&
          !this.bossFinalstarted
        ) {
          this.bossFinalBattleStart()
        }
      },
      this
    )
    this.solLayer.setTileLocationCallback(70, 127, 8, 1, null)
    this.solLayer.setTileLocationCallback(1, 188, 116, 1, null)
  }

  // Ok
  bossFinalBattleStart () {
    this.bossFinalBattlePreventSave = true
    this.bossFinal.playBossMusic()
    this.bossFinal.playRoar('cri1')
    this.time.addEvent({
      delay: 700,
      callback: () => {
        this.bossFinal.animate('bossFinalAttack')
        if (this.bossFinal.y < 2810) {
          this.bossFinal.body.velocity.y = 0
        } else if (this.bossFinal.y > 2900) {
          this.bossFinal.body.velocity.y = -300
        } else {
          this.bossFinal.body.velocity.y = 0
        }
      },
    })
    this.time.addEvent({
      delay: 3000,
      callback: () => {
        this.bossFinalstarted = true
      },
    })
  }

  saveGame () {
    if (this.boss1BattlePreventSave || this.bossFinalBattlePreventSave) {
      return
    }
    this.player.inventory.savedPositionX = this.player.x
    this.player.inventory.savedPositionY = this.player.y
    const s = JSON.stringify(this.player.inventory)
    localStorage.setItem('userSavedGame', s)
    this.sound.play('melo')
    this.countTime()
  }

  // ====================================================================
  loadGame () {
    const l = localStorage.getItem('userSavedGame')
    this.player.inventory = JSON.parse(l)
    this.player.x = this.player.inventory.savedPositionX
    this.player.y = this.player.inventory.savedPositionY
  }

  // ====================================================================

  // Ok
  transmission (txt) {
    let count = 0
    this.modalText = this.add
      .bitmapText(this.player.x, this.player.y - 480, 'atomic', '', 6, 1)
      .setOrigin(0.5, 0.5)
      .setAlpha(1)
      .setDepth(201)
    this.time.addEvent({
      delay: 100,
      repeat: txt.length - 1,
      callback: () => {
        if (txt[count] === '-') {
          this.modalText.text += '\n'
          this.modalText.y -= 10
          this.sound.play('bip3', { volume: 0.5 })
          count += 1
        } else {
          this.modalText.text += txt[count]
          this.sound.play('bip1', { volume: 1 })
          count += 1
        }
      },
    })
    this.time.addEvent({
      delay: 12000,
      callback: () => {
        this.modalText.destroy()
      },
    })
  }
}
