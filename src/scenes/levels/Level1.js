import { Scene } from 'phaser'
import gameState from '../../state/game'
import text from '../../assets/text/en.json'

import BossManager from '../../managers/BossManager'
import CamerasManager from '../../managers/CamerasManager'
import CollisionsManager from '../../managers/CollisionsManager'
import DamagesManager from '../../managers/DamagesManager'
import EnemiesManager from '../../managers/EnemiesManager'
import EnvironmentManager from '../../managers/EnvironmentManager'
import MapManager from '../../managers/MapManager'
import MessagesManager from '../../managers/MessagesManager'
import MissionManager from '../../managers/MissionManager'
import MusicManager from '../../managers/MusicManager'
import PlayerManager from '../../managers/PlayerManager'
import PowerUpsManager from '../../managers/PowerUpsManager'

export default class Level1 extends Scene {
  // Managers
  #mapManager
  #environmentManager
  #playerManager
  #powerUpsManager
  #enemiesManager
  #bossManager
  #musicManager
  #collisionsManager
  #damagesManager
  #messagesManager
  #missionManager
  #camerasManager

  constructor () {
    super('level1')

    gameState.scene = this

    this.#mapManager = new MapManager()
    this.#environmentManager = new EnvironmentManager()
    this.#playerManager = new PlayerManager()
    this.#powerUpsManager = new PowerUpsManager()
    this.#enemiesManager = new EnemiesManager()
    this.#bossManager = new BossManager()
    this.#musicManager = new MusicManager()
    this.#collisionsManager = new CollisionsManager()
    this.#damagesManager = new DamagesManager()
    this.#missionManager = new MissionManager()
    this.#messagesManager = new MessagesManager()
    this.#camerasManager = new CamerasManager()
  }

  create () {
    this.#initManagers()
    this.events.emit('loadingDone')
    this.transmit(text.level1.message_intro)

    if (this.data.systems.settings.data.loadSavedGame) {
      this.mission.loadGame()
    }
  }

  #initManagers () {
    this.#missionManager.init()
    this.#mapManager.init()
    this.#playerManager.init()
    this.#enemiesManager.init()
    this.#environmentManager.init()
    this.#bossManager.init()
    this.#musicManager.init()
    this.#powerUpsManager.init()
    this.#camerasManager.init()
    this.#collisionsManager.init()
  }

  update () {
    this.#musicManager.onUpdate()
    this.#environmentManager.onUpdate()
    this.#playerManager.onUpdate()
    this.#messagesManager.onUpdate()
    this.#camerasManager.onUpdate()

    if (this.plugins.scenePlugins.includes('CustomDebugger')) {
      this.events.emit('updated')
    }
  }

  get boss () {
    return this.#bossManager
  }

  get camera () {
    return this.#camerasManager
  }

  get damages () {
    return this.#damagesManager
  }

  get environment () {
    return this.#environmentManager
  }

  get mission () {
    return this.#missionManager
  }

  get pm () {
    return this.#playerManager
  }

  get player () {
    return gameState.player
  }

  get state () {
    return gameState
  }

  getPowerUp (element) {
    this.#powerUpsManager.getPowerUp(element)
  }

  transmit (message) {
    this.#messagesManager.transmit(message)
  }

  notifyPowerUp (element) {
    this.#messagesManager.notifyPowerUp(element)
  }
}
