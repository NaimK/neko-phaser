import Phaser, { Input } from 'phaser'

export default class DevMode extends Phaser.Plugins.BasePlugin {
  keys = []
  constructor (scene) {
    super(scene)

    const { KeyCodes } = Input.Keyboard

    this.keys = {
      shift: KeyCodes.SHIFT,
      // positions
      zero: KeyCodes.ZERO,
      one: KeyCodes.ONE,
      two: KeyCodes.TWO,
      three: KeyCodes.THREE,
      four: KeyCodes.FOUR,
      five: KeyCodes.FIVE,
      six: KeyCodes.SIX,
      seven: KeyCodes.SEVEN,
      eight: KeyCodes.EIGHT,
      nine: KeyCodes.NINE,
      // other
      c: KeyCodes.C,
      d: KeyCodes.D,
      e: KeyCodes.E,
      i: KeyCodes.I,
      n: KeyCodes.N,
      u: KeyCodes.U,
    }

    const { shift, zero, one, two, three, four, five, six, seven, eight, nine, c, d, e, i, n, u } = this.keys

    if (scene.sys.settings.key === 'level1') {
      console.info('DEV MODE ON')
      console.info('SHORTCUTS TO USE ON THE MAIN MAP:')
      console.table([
        {
          shortcut: 'CTRL + [Any number betwen 0 and 9]',
          action: 'Go to another place on the map',
        },
        {
          shortcut: 'CTRL + (C * 2)',
          action: 'Change main camera zoom',
        },
        {
          shortcut: 'CTRL + (E * 2)',
          action: 'Weak enemies',
        },
        {
          shortcut: 'CTRL + (U * 2)',
          action: 'Player gets all weapons and power ups',
        },
        {
          shortcut: 'CTRL + E N D',
          action: 'Go to end screen',
        },
        {
          shortcut: 'CTRL + D I E',
          action: 'Go to game over screen',
        },
      ])

      scene.input.keyboard.createCombo(
        [shift, zero],
        {
          resetOnWrongKey: true,
          resetOnMatch: true,
        }
      )
      scene.input.keyboard.createCombo(
        [shift, one],
        {
          resetOnWrongKey: true,
          resetOnMatch: true,
        }
      )
      scene.input.keyboard.createCombo(
        [shift, two],
        {
          resetOnWrongKey: true,
          resetOnMatch: true,
        }
      )
      scene.input.keyboard.createCombo(
        [shift, three],
        {
          resetOnWrongKey: true,
          resetOnMatch: true,
        }
      )
      scene.input.keyboard.createCombo(
        [shift, four],
        {
          resetOnWrongKey: true,
          resetOnMatch: true,
        }
      )
      scene.input.keyboard.createCombo(
        [shift, five],
        {
          resetOnWrongKey: true,
          resetOnMatch: true,
        }
      )
      scene.input.keyboard.createCombo(
        [shift, six],
        {
          resetOnWrongKey: true,
          resetOnMatch: true,
        }
      )
      scene.input.keyboard.createCombo(
        [shift, seven],
        {
          resetOnWrongKey: true,
          resetOnMatch: true,
        }
      )
      scene.input.keyboard.createCombo(
        [shift, eight],
        {
          resetOnWrongKey: true,
          resetOnMatch: true,
        }
      )
      scene.input.keyboard.createCombo(
        [shift, nine],
        {
          resetOnWrongKey: true,
          resetOnMatch: true,
        }
      )
      scene.input.keyboard.createCombo(
        [shift, c, c],
        {
          resetOnWrongKey: true,
          resetOnMatch: true,
        }
      )
      scene.input.keyboard.createCombo(
        [shift, u, u],
        {
          resetOnWrongKey: true,
          resetOnMatch: true,
        }
      )
      scene.input.keyboard.createCombo(
        [shift, e, e],
        {
          resetOnWrongKey: true,
          resetOnMatch: true,
        }
      )
      scene.input.keyboard.createCombo(
        [shift, e, n, d],
        {
          resetOnWrongKey: true,
          resetOnMatch: true,
        }
      )
      scene.input.keyboard.createCombo(
        [shift, d, i, e],
        {
          resetOnWrongKey: true,
          resetOnMatch: true,
        }
      )
    }

    scene.input.keyboard.on('keycombomatch', (keyCombo) => {
      if (keyCombo.keyCodes.includes(shift)) {
        // ===================
        // POSITIONS
        // ===================
        // Actual player start position
        if (keyCombo.keyCodes.includes(zero)) {
          scene.player.x = 1924
          scene.player.y = 158
        }
        // Morphing ball
        if (keyCombo.keyCodes.includes(one)) {
          scene.player.x = 250
          scene.player.y = 440
        }
        // Octopus energy
        if (keyCombo.keyCodes.includes(two)) {
          scene.player.x = 40
          scene.player.y = 820
        }
        // jumpers
        if (keyCombo.keyCodes.includes(three)) {
          scene.player.x = 1900
          scene.player.y = 600
        }
        // door 5
        if (keyCombo.keyCodes.includes(four)) {
          scene.player.x = 1100
          scene.player.y = 1000
        }
        // elevator 1
        if (keyCombo.keyCodes.includes(five)) {
          scene.player.x = 1175
          scene.player.y = 980
        }
        // boss 1
        if (keyCombo.keyCodes.includes(six)) {
          scene.player.x = 1325
          scene.player.y = 1260
        }
        // Rhinos
        if (keyCombo.keyCodes.includes(seven)) {
          scene.player.x = 860
          scene.player.y = 1650
        }
        // secret place
        if (keyCombo.keyCodes.includes(eight)) {
          scene.player.x = 2000
          scene.player.y = 1575
        }
        // boss final
        if (keyCombo.keyCodes.includes(nine)) {
          scene.player.x = 1000
          scene.player.y = 3000
        }
        if (!keyCombo.keyCodes.includes(two)) {
          scene.player.onWater = false
        }
        // ===================
        // MISC
        // ===================
        // Camera zoom
        if (keyCombo.keyCodes.includes(c)) {
          const currentZoom = scene.cameras.main.zoom
          const zoomValues = [2, 1, 0.5, 0.25]

          const index = zoomValues.indexOf(currentZoom)
          let newIndex

          if (index === zoomValues.length - 1) {
            newIndex = 0
          } else {
            newIndex = index + 1
          }
          scene.cameras.main.setZoom(zoomValues[newIndex])
          console.log('Camera zoom value modified')
        }
        // Cheat mode on
        if (keyCombo.keyCodes.includes(u)) {
          scene.events.emit('setHealth', { life: 2000 })
          scene.player.addSpeedFire()
          scene.player.addMissile()
          scene.player.addLaser()
          scene.player.addSwell()
          scene.player.inventory.morphing = true
          scene.player.inventory.morphingBomb = true
          scene.player.inventory.morphingSonar = true
          scene.player.inventory.jumpBooster = true
          scene.sound.play('powerUp')
          console.log('Player gets all weapons and power ups activated')
        }
        // Weak enemies
        if (keyCombo.keyCodes.includes(e)) {
          scene.state.enemyGroup.forEach(enemy => {
            enemy.state.life = 1
            enemy.state.damage = 1
          })
          if (scene.state.boss1) {
            scene.state.boss1.state.life = 100
            scene.state.boss1.state.damage = 1
          }
          if (scene.state.bossFinal) {
            scene.state.bossFinal.state.life = 100
            scene.state.bossFinal.state.damage = 1
            scene.state.bossFinal.state.fireRate = 1
          }
          console.log('Weak enemies activated for enemies already on map')
        }
        if (keyCombo.keyCodes.includes(d) && keyCombo.keyCodes.includes(i) && keyCombo.keyCodes.includes(e)) {
          console.log('Dying ! Nooo...')
          scene.scene.start('gameOver')
        }
        if (keyCombo.keyCodes.includes(e) && keyCombo.keyCodes.includes(n) && keyCombo.keyCodes.includes(d)) {
          console.log('Going to end screen')
          scene.scene.start('endGame')
        }
      }
    })
  }
}
