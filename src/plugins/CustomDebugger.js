import Phaser from 'phaser'

export default class CustomDebugger extends Phaser.Plugins.BasePlugin {
  constructor (scene) {
    super(scene)
    if (scene.sys.settings.key === 'level1') {
      setTimeout(() => {
        scene.pointer = scene.input.activePointer

        scene.events.on('updated', () => {
          console.log(scene.pointer.x, scene.pointer.y)
        })
      }, 100)
    }
  }
}
