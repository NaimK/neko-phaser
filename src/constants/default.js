export const DEFAULT_KEYS = {
  left: 'LEFT',
  right: 'RIGHT',
  up: 'UP',
  down: 'DOWN',
  fire: 'ENTER',
  jump: 'SPACE',
  run: 'SHIFT',
  selectWeapon: 'S',
  pauseSave: 'P',
}

export const DEFAULT_PLAYER_STATE = {
  LIFE: 100,
  SELECTABLE_WEAPONS: ['bullet'],
  GUN: false,
  SWELL: false,
  MISSILE: false,
  LASER: false,
  MORPHING: false,
  MORPHING_BOMB: false,
  MORPHING_SONAR: false,
  JUMP_BOOSTER: false,
  BOSS_1: false,
  BOSS_FINAL: false,
  RHINO: false,
}
