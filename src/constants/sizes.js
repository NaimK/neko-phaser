export default {
  GAME_WIDTH: 800,
  GAME_HEIGHT: 512,
}

export const GAME_WIDTH = 800
export const GAME_HEIGHT = 512

export const GUTTER = 25

export const PLAYER_MESSAGE_OFFSET = 75
export const POWER_UP_MESSAGE_OFFSET = 50
