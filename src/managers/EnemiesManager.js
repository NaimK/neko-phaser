import Manager from './Manager'
import EnemiesFactory from '../factories/EnemiesFactory'
import gameState from '../state/game'

export default class EnemiesManager extends Manager {
  init () {
    this.#setAnimations()
    this.#createEnemies()
  }

  #setAnimations () {
    gameState.explodeSprite = this.$scene.add.group({
      defaultKey: 'transparentPixel',
      maxSize: 30,
      allowGravity: false,
      createIfNull: true,
    })

    // anims enemies
    this.createAnimationFrom('enemyExplode', 'enemyExplode', {
      start: 0,
      end: 4,
      frameRate: 15,
      repeat: 0,
    })
    this.createAnimationFrom('bossExplode', 'enemyExplode', {
      start: 0,
      end: 4,
      frameRate: 15,
      repeat: 10,
    })
    this.createAnimationFrom('crabe', 'crabe', {
      start: 0,
      end: 3,
    })
    this.createAnimationFrom('guepe', 'guepe', {
      start: 0,
      end: 2,
    })
    this.createAnimationFrom('guepe2', 'guepe2', {
      start: 0,
      end: 2,
    })
    this.createAnimationFrom('jumper1Idle', 'jumper1', {
      start: 0,
      end: 3,
    })
    this.createAnimationFrom('jumper1Jump', 'jumper1', {
      start: 4,
      end: 4,
      repeat: 0,
    })
    this.createAnimationFrom('jumper2Idle', 'jumper2', {
      start: 0,
      end: 3,
    })
    this.createAnimationFrom('jumper2Jump', 'jumper2', {
      start: 4,
      end: 4,
      first: 4,
      repeat: 0,
    })
    this.createAnimationFrom('octopus', 'octopus', {
      start: 0,
      end: 3,
      yoyo: true,
    })
    this.createAnimationFrom('octopusIdle', 'octopus', {
      start: 2,
      end: 2,
      repeat: 0,
    })
    this.createAnimationFrom('fireball', 'fireball', {
      start: 0,
      end: 2,
      frameRate: 5,
    })
    this.createAnimationFrom('rhinoWalk', 'rhinobeetle', {
      start: 0,
      end: 2,
      frameRate: 5,
    })
    this.createAnimationFrom('rhinoBall', 'rhinobeetle', {
      start: 3,
      end: 6,
      frameRate: 5,
    })
  }

  #createEnemies () {
    gameState.enemyGroup = []

    gameState.enemyGroup.push(
      ...EnemiesFactory.make('Crabes', gameState.map.objects[1].objects),
      ...EnemiesFactory.make('Guepes', gameState.map.objects[3].objects),
      ...EnemiesFactory.make('Jumpers', gameState.map.objects[4].objects),
      ...EnemiesFactory.make('Octopus', gameState.map.objects[9].objects),
    )

    // the rhinobeetles
    this.loadRhino = false
    if (!gameState.player.inventory.rhino) {
      gameState.solLayer.setTileLocationCallback(
        53,
        103,
        1,
        8,
        (e) => {
          if (e === gameState.player && !this.loadRhino) {
            this.loadRhino = true
            gameState.enemyGroup.push(
              ...EnemiesFactory.make('RhinoBeetles', gameState.map.objects[11].objects),
            )
          }
        }
      )
    }
  }
}
