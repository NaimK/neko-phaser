import Phaser from 'phaser'
import Manager from './Manager'
import gameState from '../state/game'

import Elevators from '../sprites/environment/Elevators'
import Lava from '../sprites/environment/Lava'
import WaterFalls from '../sprites/environment/WaterFalls'
import Doors from '../sprites/environment/Doors'
import FireBalls from '../sprites/enemies/FireBalls'

export default class StageSetManager extends Manager {
  init () {
    this.#setElevators()
    this.#setLava()
    this.#setWaterfall()
    this.#setDoors()
  }

  onUpdate () {
    if (gameState.lavaRise) {
      this.#stopLavaRise()
      this.#updateSismicActivity()
    }
  }

  #setElevators () {
    gameState.elevatorGroup = []
    gameState.map.objects[2].objects.forEach((element) => {
      this.$scene[element.name] = new Elevators(this.$scene, element.x + 24, element.y, {
        key: element.properties.key,
        up: element.properties.up,
        down: element.properties.down,
        position: element.properties.position,
      })
      gameState.elevatorGroup.push(this.$scene[element.name])
    })
  }

  #setLava () {
    this.createAnimationFrom('lava', 'lava', {
      start: 0,
      end: 2,
      frameRate: 2,
    })
    gameState.lavaGroup = []
    gameState.map.objects[7].objects.forEach((element) => {
      this.$scene[element.name] = new Lava(this.$scene, element.x, element.y, {
        key: element.properties.key,
      })
      this.$scene[element.name].animate(element.properties.key, true)
      this.$scene[element.name].setDepth(11)
      gameState.lavaGroup.push(this.$scene[element.name])
    })
    // lava fall, same group as lava
    this.createAnimationFrom('lavaFall', 'lavaFall', {
      start: 0,
      end: 2,
      frameRate: 3,
    })
    gameState.map.objects[6].objects.forEach((element) => {
      this.$scene[element.name] = new Lava(this.$scene, element.x + 16, element.y - 8, {
        key: element.properties.key,
      })
      this.$scene[element.name].setDisplaySize(32, 32).setDepth(10)
      this.$scene[element.name].animate(element.properties.key, true)
      gameState.lavaGroup.push(this.$scene[element.name])
    })
    // fireballs, same group as lava
    gameState.map.objects[10].objects.forEach((element) => {
      this.$scene[element.name] = new FireBalls(this.$scene, element.x + 16, element.y - 8, {
        key: element.properties.key,
      })
      this.$scene[element.name].animate(element.properties.key, true)
      gameState.lavaGroup.push(this.$scene[element.name])
    })

    // LAVA RISE
    gameState.lavaRiseFlag = false
    gameState.onSismic = false
    gameState.isTheEnd = false // My only friend, the end
  }

  #setWaterfall () {
    this.createAnimationFrom('waterFall', 'waterFall', {
      start: 0,
      end: 2,
      frameRate: 6,
    })
    gameState.map.objects[5].objects.forEach((element) => {
      this.$scene[element.name] = new WaterFalls(this.$scene, element.x + 8, element.y - 8, {
        key: element.properties.key,
      })
      this.$scene[element.name].animate(element.properties.key, true)
    })
  }

  #setDoors () {
    gameState.doorGroup = []
    let door
    gameState.map.objects[8].objects.forEach((element, i) => {
      if (element.properties.side === 'right') {
        door = new Doors(this.$scene, element.x + 3, element.y + 9, {
          key: element.properties.key,
          side: element.properties.side,
          name: element.name,
        })
        door.body.setSize(10, 47)
      }
      if (element.properties.side === 'left') {
        door = new Doors(this.$scene, element.x + 16, element.y + 9, {
          key: element.properties.key,
          side: element.properties.side,
          name: element.name,
        })
        door.flipX = true
        door.body.setSize(10, 47)
      }
      gameState.doorGroup.push(door)
    })
    gameState.boss1Door = gameState.doorGroup.find(door => door.name === 'door7')
    gameState.boss1Door.body.enable = false
    gameState.boss1Door.alpha = 0
  }

  startLavaRise () {
    this.$scene.transmit(
      'ALERT-High sismic activity detected-return to spacehip for evacuation'
    )
    gameState.solLayer.setTileLocationCallback(
      119,
      3,
      1,
      9,
      (e) => {
        if (e === gameState.player) {
          this.$scene.mission.endMission()
        }
      },
      this
    )
    this.$scene.time.addEvent({
      delay: 5000,
      callback: () => {
        gameState.lavaRise = this.$scene.physics.add
          .image(0, 3072, 'lavaPixel')
          .setOrigin(0, 0)
          .setDisplaySize(2048, 3072)
          .setDepth(99)
          .setAlpha(0.9)
          .setOffset(0, 0)
        gameState.lavaRise.body
          .setVelocityY(-43)
          .setImmovable(true)
          .setAllowGravity(false)
        this.$scene.physics.add.overlap(
          gameState.player,
          gameState.lavaRise,
          () => gameState.player.handleLava(),
          null,
          gameState.player
        )
      },
    })
  }

  openDoor (d, miss) {
    gameState.player.missileKill(miss)
    d.destroyDoor()
  }

  #stopLavaRise () {
    if (!gameState.lavaRiseFlag && gameState.lavaRise.y > 0) {
      gameState.lavaRiseFlag = true
    }
    if (gameState.lavaRise.y < 0) {
      gameState.lavaRise.setVelocityY(0)
    }
  }

  #updateSismicActivity () {
    if (!gameState.onSismic) {
      gameState.onSismic = true
      const rdm = Phaser.Math.Between(2000, 5000)
      this.$scene.camera.shakeCamera(1000)
      this.$scene.sound.play('shake', { volume: 0.5 })
      this.$scene.time.addEvent({
        delay: rdm,
        callback: () => {
          gameState.onSismic = false
        },
      })
    }
  }
}
