import Manager from './Manager'
import gameState from '../state/game'

export default class MapManager extends Manager {
  init () {
    this.#setCameras()
  }

  onUpdate () {
    gameState.camPosition = this.$scene.cameras.main.midPoint
  }

  #setCameras () {
    // set bounds so the camera won't go outside the game world
    this.$scene.cameras.main.setBounds(
      0,
      0,
      gameState.map.widthInPixels,
      gameState.map.heightInPixels
    )
    // make the camera follow the player
    this.$scene.cameras.main.startFollow(gameState.player, true, 0.4, 0.1)
    this.$scene.cameras.main.transparent = true
    this.$scene.cameras.main.setZoom(2)
    this.$scene.cameras.main.fadeIn(200)
  }

  shakeCamera (e) {
    this.$scene.cameras.main.shake(e, 0.005)
  }

  flashCamera () {
    this.$scene.cameras.main.flash(1000)
  }
}
