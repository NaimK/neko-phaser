import Manager from './Manager'
import gameState from '../state/game'

export default class MusicManager extends Manager {
  init () {
    this.#setAmbientMusic()
  }

  onUpdate () {
    this.#playMusic()
  }

  #setAmbientMusic () {
    gameState.ambient1 = this.$scene.sound.add('ambient1', { volume: 0.2 })
    gameState.ambient2 = this.$scene.sound.add('ambient2', { volume: 0.1 })
    gameState.ambient3 = this.$scene.sound.add('ambient3', { volume: 0.1 })
  }

  #playMusic () {
    if (gameState.player.y <= 1024 && !gameState.isTheEnd) {
      if (gameState.ambient2.isPlaying) {
        gameState.ambient2.stop()
      }
      if (!gameState.ambient1.isPlaying) {
        gameState.ambient1.play()
      }
    }
    if (gameState.player.y <= 2080 && gameState.player.y > 1024) {
      if (gameState.ambient1.isPlaying || gameState.ambient3.isPlaying) {
        gameState.ambient1.stop()
        gameState.ambient3.stop()
      }
      if (!gameState.ambient2.isPlaying) {
        gameState.ambient2.play()
      }
    }
    if (gameState.player.y <= 3056 && gameState.player.y > 2080) {
      if (gameState.ambient2.isPlaying) {
        gameState.ambient2.stop()
      }
      if (!gameState.ambient3.isPlaying && !gameState.bossMusic.isPlaying) {
        gameState.ambient3.play()
      }
    }
  }
}
