import Phaser from 'phaser'
import Manager from './Manager'
import gameState from '../state/game'

import { countDeadEnemies } from '../helpers/stats'
import { countPlayedTime } from '../helpers/time'

export default class DamagesManager extends Manager {
  // ====================================================================
  playerIsHit (elm) {
    if (!gameState.playerHurt) {
      gameState.playerHurt = true // flag
      // gameState.player.animate('hurt');
      this.$scene.sound.play('playerHit')
      gameState.player.inventory.life -= elm.state.damage
      gameState.playerFlashTween = this.$scene.tweens.add({
        targets: gameState.player,
        ease: 'Sine.easeInOut',
        duration: 200,
        delay: 0,
        repeat: 5,
        yoyo: true,
        alpha: {
          getStart: () => 0,
          getEnd: () => 1,
        },
        onComplete: () => {
          gameState.player.alpha = 1
          gameState.playerHurt = false
          // gameState.player.animate('stand');
        },
      })
      // if player is dead, launch death sequence
      if (gameState.player.inventory.life <= 0) {
        gameState.player.state.dead = true
        gameState.playerDead = true
        this.$scene.physics.pause()
        this.$scene.input.enabled = false
        gameState.player.anims.pause(gameState.player.anims.currentFrame)
        gameState.playerFlashTween.stop()
        gameState.player.inventory.life = 0
        gameState.player.setTintFill(0xffffff)
        gameState.player.setDepth(2000)

        gameState.round = this.$scene.add.sprite(
          gameState.player.x,
          gameState.player.y,
          'whitePixel'
        )
        gameState.round.setOrigin(0.5, 0.5)
        gameState.round.setDepth(1000)
        gameState.round.displayWidth = 2
        gameState.round.displayHeight = 2
        this.$scene.sound.play('playerDead', { volume: 0.2 })

        this.$scene.tween = this.$scene.tweens.add({
          targets: gameState.round,
          ease: 'Sine.easeInOut',
          scaleX: 1,
          scaleY: 1,
          duration: 2500,
          delay: 800,
          onComplete: () => {
            this.$scene.input.enabled = true
            this.playerIsDead()
          },
        })
      }
    }
    this.$scene.events.emit('setHealth', { life: gameState.player.inventory.life }) // set health dashboard scene
  }

  // ====================================================================
  playerIsDead () {
    let d = localStorage.getItem('d')
    d = JSON.parse(d)
    d += 1
    localStorage.setItem('d', d)
    gameState.bossMusic.stop()
    gameState.ambient1.stop()
    gameState.ambient2.stop()
    gameState.ambient3.stop()
    gameState.waterAmbientMusic.stop()

    countPlayedTime(gameState.levelStartTimestamp)

    if (gameState.lavaRise) {
      gameState.lavaRise = null
    }
    gameState.player.state.dead = false
    this.$scene.scene.start('gameOver')
  }

  // ====================================================================
  enemyIsHit (bull, elm) {
    const el = elm
    if (!el.getFired) {
      el.getFired = true
      if (
        gameState.player.state.selectedWeapon === 'missile' ||
        gameState.player.state.selectedWeapon === 'bullet' ||
        gameState.player.state.selectedWeapon === 'swell'
      ) {
        gameState.player[`${gameState.player.state.selectedWeapon}Kill`](bull)
      }
      if (el === this.$scene.rhino1 || el === this.$scene.rhino2 || el === this.$scene.rhino3) {
        if (el.vulnerable) {
          el.looseLife(
            gameState.player.inventory[`${gameState.player.state.selectedWeapon}Damage`]
          )
          el.setTintFill(0xdddddd)
          this.$scene.time.addEvent({
            delay: 50,
            callback: () => {
              el.clearTint()
            },
          })
        }
      } else {
        el.looseLife(
          gameState.player.inventory[`${gameState.player.state.selectedWeapon}Damage`]
        )
        el.setTintFill(0xdddddd)
        this.$scene.time.addEvent({
          delay: 50,
          callback: () => {
            el.clearTint()
          },
        })
      }
      this.$scene.hitTimer = this.$scene.time.addEvent({
        delay: 120,
        callback: () => {
          el.getFired = false
        },
      })
    }
    if (el.state.life < 0) {
      el.clearTint()
      // kill the enemy
      if (el === this.$scene.rhino1 || el === this.$scene.rhino2 || el === this.$scene.rhino3) {
        gameState.giveLife = this.$scene.physics.add.staticSprite(el.x, el.y, 'powerUp')
        gameState.giveLife.setDepth(105)
        gameState.giveLife.health = el.state.giveLife
        gameState.giveLife.body.setSize(23, 21)
        gameState.giveLife.anims.play('powerUp')
        gameState.giveLifeGroup.push(gameState.giveLife)
        countDeadEnemies()
        gameState.player.state.rhinoCount += 1
        if (gameState.player.state.rhinoCount === 3) {
          gameState.player.inventory.rhino = true
        }
      }
      if (el === gameState.boss1) {
        gameState.giveLife = this.$scene.physics.add.staticSprite(el.x, el.y, 'powerUp')
        gameState.giveLife.setDepth(105)
        gameState.giveLife.health = el.state.giveLife
        gameState.giveLife.body.setSize(23, 21)
        gameState.giveLife.anims.play('powerUp')
        gameState.giveLifeGroup.push(gameState.giveLife)
        gameState.boss1.setTintFill(0xdddddd)
        this.$scene.missile.body.reset(gameState.boss1.x, gameState.boss1.y)
        this.bossExplode(gameState.boss1.x, gameState.boss1.y)
        this.$scene.camera.shakeCamera(5000)
        this.$scene.time.addEvent({
          delay: Phaser.Math.Between(500, 1200),
          callback: () => {
            this.bossExplode(
              gameState.boss1.x - 50,
              Phaser.Math.Between(gameState.boss1.y - 50, gameState.boss1.y + 50)
            )
            this.bossExplode(
              gameState.boss1.x - 20,
              Phaser.Math.Between(gameState.boss1.y - 50, gameState.boss1.y + 50)
            )
          },
        })
        this.$scene.time.addEvent({
          delay: Phaser.Math.Between(500, 1200),
          callback: () => {
            this.bossExplode(
              gameState.boss1.x,
              Phaser.Math.Between(gameState.boss1.y - 50, gameState.boss1.y + 50)
            )
            this.bossExplode(
              gameState.boss1.x + 20,
              Phaser.Math.Between(gameState.boss1.y - 50, gameState.boss1.y + 50)
            )
          },
        })
        this.$scene.time.addEvent({
          delay: Phaser.Math.Between(500, 1200),
          callback: () => {
            this.bossExplode(
              gameState.boss1.x + 50,
              Phaser.Math.Between(gameState.boss1.y - 50, gameState.boss1.y + 50)
            )
            this.bossExplode(
              gameState.boss1.x - 50,
              Phaser.Math.Between(gameState.boss1.y - 50, gameState.boss1.y + 50)
            )
          },
        })
        this.$scene.time.addEvent({
          delay: Phaser.Math.Between(500, 1200),
          callback: () => {
            this.bossExplode(
              gameState.boss1.x,
              Phaser.Math.Between(gameState.boss1.y - 50, gameState.boss1.y + 50)
            )
            this.bossExplode(
              gameState.boss1.x,
              Phaser.Math.Between(gameState.boss1.y - 50, gameState.boss1.y + 50)
            )
          },
        })
        this.$scene.time.addEvent({
          delay: Phaser.Math.Between(500, 1200),
          callback: () => {
            this.bossExplode(
              gameState.boss1.x + 20,
              Phaser.Math.Between(gameState.boss1.y - 50, gameState.boss1.y + 50)
            )
            this.bossExplode(
              gameState.boss1.x + 50,
              Phaser.Math.Between(gameState.boss1.y - 50, gameState.boss1.y + 50)
            )
          },
        })
        countDeadEnemies()
        gameState.boss1.anims.pause(gameState.boss1.anims.currentFrame)
        gameState.boss1.body.setEnable(false)
        gameState.boss1.setDepth(0)
        gameState.boss1BattlePreventSave = false
        this.$scene.time.addEvent({
          delay: 600,
          callback: () => {
            this.$scene.tween = this.$scene.tweens.add({
              targets: gameState.boss1,
              ease: 'Sine.easeInOut',
              duration: 1000,
              delay: 0,
              repeat: 0,
              yoyo: false,
              alpha: {
                getStart: () => 1,
                getEnd: () => 0,
              },
              onComplete: () => {
                this.$scene.missile.alpha = 1
                gameState.player.inventory.boss1 = true
                gameState.boss1.destroy()
              },
            })
          },
        })
      } else if (el === gameState.bossFinal) {
        if (!gameState.bossFinal.isDead) {
          gameState.bossFinal.isDead = true
          gameState.bossFinal.body.setVelocityY(500)
          gameState.bossFinal.setTintFill(0xdddddd)
          this.bossExplode(gameState.bossFinal.x, gameState.bossFinal.y)
          this.$scene.camera.shakeCamera(5000)
          this.$scene.time.addEvent({
            delay: Phaser.Math.Between(500, 1200),
            callback: () => {
              this.bossExplode(
                gameState.bossFinal.x - 50,
                Phaser.Math.Between(
                  gameState.bossFinal.y - 50,
                  gameState.bossFinal.y + 50
                )
              )
              this.bossExplode(
                gameState.bossFinal.x - 20,
                Phaser.Math.Between(
                  gameState.bossFinal.y - 50,
                  gameState.bossFinal.y + 50
                )
              )
            },
          })
          this.$scene.time.addEvent({
            delay: Phaser.Math.Between(500, 1200),
            callback: () => {
              this.bossExplode(
                gameState.bossFinal.x,
                Phaser.Math.Between(
                  gameState.bossFinal.y - 50,
                  gameState.bossFinal.y + 50
                )
              )
              this.bossExplode(
                gameState.bossFinal.x + 20,
                Phaser.Math.Between(
                  gameState.bossFinal.y - 50,
                  gameState.bossFinal.y + 50
                )
              )
            },
          })
          this.$scene.time.addEvent({
            delay: Phaser.Math.Between(500, 1200),
            callback: () => {
              this.bossExplode(
                gameState.bossFinal.x + 50,
                Phaser.Math.Between(
                  gameState.bossFinal.y - 50,
                  gameState.bossFinal.y + 50
                )
              )
              this.bossExplode(
                gameState.bossFinal.x - 50,
                Phaser.Math.Between(
                  gameState.bossFinal.y - 50,
                  gameState.bossFinal.y + 50
                )
              )
            },
          })
          this.$scene.time.addEvent({
            delay: Phaser.Math.Between(500, 1200),
            callback: () => {
              this.bossExplode(
                gameState.bossFinal.x,
                Phaser.Math.Between(
                  gameState.bossFinal.y - 50,
                  gameState.bossFinal.y + 50
                )
              )
              this.bossExplode(
                gameState.bossFinal.x,
                Phaser.Math.Between(
                  gameState.bossFinal.y - 50,
                  gameState.bossFinal.y + 50
                )
              )
            },
          })
          this.$scene.time.addEvent({
            delay: Phaser.Math.Between(500, 1200),
            callback: () => {
              this.bossExplode(
                gameState.bossFinal.x + 20,
                Phaser.Math.Between(
                  gameState.bossFinal.y - 50,
                  gameState.bossFinal.y + 50
                )
              )
              this.bossExplode(
                gameState.bossFinal.x + 50,
                Phaser.Math.Between(
                  gameState.bossFinal.y - 50,
                  gameState.bossFinal.y + 50
                )
              )
            },
          })
          gameState.bossFinal.anims.pause(gameState.bossFinal.anims.currentFrame)
          gameState.bossFinal.setDepth(0)
          gameState.bossFinal.playRoar('cri4')
          gameState.bossFinal.body.velocity.y = 500
          this.$scene.time.addEvent({
            delay: 600,
            callback: () => {
              this.$scene.tween = this.$scene.tweens.add({
                targets: gameState.bossFinal,
                ease: 'Sine.easeInOut',
                duration: 1000,
                delay: 0,
                repeat: 0,
                yoyo: false,
                alpha: {
                  getStart: () => 1,
                  getEnd: () => 0,
                },
                onComplete: () => {
                  countDeadEnemies()
                  gameState.giveLife = this.$scene.physics.add.staticSprite(
                    gameState.bossFinal.x,
                    gameState.bossFinal.y,
                    'powerUp'
                  )
                  gameState.giveLife.setDepth(105)
                  gameState.giveLife.health = el.state.giveLife
                  gameState.giveLife.body.setSize(23, 21)
                  gameState.giveLife.anims.play('powerUp')
                  gameState.giveLifeGroup.push(gameState.giveLife)
                  const filteringOptions = {
                    // isNotEmpty: false,
                    isColliding: false,
                    // hasInterestingFace: false
                  }
                  const bossFinalTiles = gameState.solLayer.getTilesWithinWorldXY(
                    gameState.bossFinal.x,
                    gameState.bossFinal.y + 48,
                    48,
                    16,
                    filteringOptions
                  )
                  bossFinalTiles.forEach((e) => {
                    if (e.properties.boss) {
                      gameState.solLayer.removeTileAt(e.x, e.y, true, true)
                    }
                  })
                  this.enemyExplode(117 * 16, 186 * 16)
                  gameState.solLayer.removeTileAt(117, 186, true, true)
                  this.enemyExplode(117 * 16, 187 * 16)
                  gameState.solLayer.removeTileAt(117, 187, true, true)
                  this.enemyExplode(117 * 16, 188 * 16)
                  gameState.solLayer.removeTileAt(117, 188, true, true)
                  this.enemyExplode(118 * 16, 186 * 16)
                  gameState.solLayer.removeTileAt(118, 186, true, true)
                  this.enemyExplode(118 * 16, 187 * 16)
                  gameState.solLayer.removeTileAt(118, 187, true, true)
                  this.enemyExplode(118 * 16, 188 * 16)
                  gameState.solLayer.removeTileAt(118, 188, true, true)
                  this.$scene.environment.startLavaRise()
                  gameState.player.inventory.bossFinal = true
                  gameState.bossFinal.destroy()
                },
              })
            },
          })
        }
      } else {
        gameState.giveLife = this.$scene.physics.add.staticSprite(el.x, el.y, 'powerUp')
        gameState.giveLife.setDepth(105)
        gameState.giveLife.health = el.state.giveLife
        gameState.giveLife.body.setSize(23, 21)
        gameState.giveLife.anims.play('powerUp')
        gameState.giveLifeGroup.push(gameState.giveLife)
        this.enemyExplode(el.x, el.y)
        this.enemyDestroy(el)
      }
    }
  }

  enemyDestroy (e) {
    e.destroy()
    countDeadEnemies()
  }

  bossExplode (x, y) {
    gameState.bossMusic.stop()
    const exp = gameState.explodeSprite
      .getFirstDead(true, x, y, 'enemyExplode', null, true)
      .setDepth(107)
    this.$scene.sound.play('explo2', { volume: 0.3 })
    if (exp) {
      exp.anims
        .play('bossExplode')
        .on('animationrepeat', () => {
          this.$scene.sound.play('explo2', { volume: 0.3 })
        })
        .on('animationcomplete', () => {
          exp.destroy()
        })
    }
  }

  enemyExplode (x, y) {
    const exp = gameState.explodeSprite
      .getFirstDead(true, x, y, 'enemyExplode', null, true)
      .setDepth(107)
    exp.anims.play('enemyExplode').on('animationcomplete', () => {
      exp.destroy()
    })
  }
}
