import Manager from './Manager'
import gameState from '../state/game'
import { PLAYER_MESSAGE_OFFSET, POWER_UP_MESSAGE_OFFSET } from '../constants/sizes'
export default class MessagesManager extends Manager {
  onUpdate () {
    if (gameState.displayPowerUpMsg) {
      gameState.powerUpText.x = gameState.player.x
      gameState.powerUpText.y = gameState.player.y - POWER_UP_MESSAGE_OFFSET
    }
    if (gameState.modalText) {
      gameState.modalText.x = gameState.player.x
      gameState.modalText.y = gameState.player.y - PLAYER_MESSAGE_OFFSET
    }
  }

  transmit (text) {
    let count = 0
    gameState.modalText = this.$scene.add
      .bitmapText(gameState.player.x, gameState.player.y - PLAYER_MESSAGE_OFFSET, 'atomic', '', 8, 1)
      .setOrigin(0.5, 0.5)
      .setAlpha(1)
      .setTint(0xffffff)
      .setDepth(201)

    this.$scene.time.addEvent({
      delay: 100,
      repeat: text.length - 1,
      callback: () => {
        if (text[count] === '-') {
          gameState.modalText.text += '\n'
          gameState.modalText.y -= 5
          this.$scene.sound.play('bip3', { volume: 0.5 })
          count += 1
        } else {
          gameState.modalText.text += text[count]
          this.$scene.sound.play('bip1', { volume: 1 })
          count += 1
        }
      },
    })
    this.$scene.time.addEvent({
      delay: 10000,
      callback: () => {
        gameState.modalText.destroy()
      },
    })
  }

  notifyPowerUp (element) {
    gameState.powerUpText = this.$scene.add
      .bitmapText(gameState.player.x, gameState.player.y - 60, 'atomic', element.state.text, 12, 1)
      .setOrigin(0.5, 0.5)
      .setAlpha(1)
      .setDepth(210)

    element.destroy()

    this.$scene.tweens.add({
      targets: [gameState.powerUpText],
      ease: 'Sine.easeInOut',
      duration: 2000,
      delay: 3000,
      repeat: 0,
      yoyo: false,
      alpha: {
        getStart: () => 1,
        getEnd: () => 0,
      },
      onComplete: () => {
        gameState.displayPowerUpMsg = false
      },
    })
  }
}
