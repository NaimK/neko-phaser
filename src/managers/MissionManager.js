import Manager from './Manager'
import gameState from '../state/game'
import { GAME_WIDTH } from '../constants/sizes'
import { countPlayedTime } from '../helpers/time'
import { PLAYER_START_X, PLAYER_START_Y } from '../constants/positions'

export default class MissionManager extends Manager {
  init () {
    gameState.levelStartTimestamp = new Date().getTime()
  }

  pauseGame () {
    if (!gameState.player.state.pause) {
      countPlayedTime(gameState.levelStartTimestamp)
      gameState.player.state.pause = true
      this.$scene.physics.pause()
      gameState.player.anims.pause(gameState.player.anims.currentFrame)
      this.$scene.msg = this.$scene.add
        .image(
          this.$scene.cameras.main.worldView.x,
          this.$scene.cameras.main.worldView.y,
          'blackPixel'
        )
        .setOrigin(0, 0)
        .setDisplaySize(400, 256)
        .setAlpha(0.9)
        .setDepth(109)

      this.$scene.msgText = this.$scene.add
        .bitmapText(
          this.$scene.cameras.main.worldView.x + 200,
          this.$scene.cameras.main.worldView.y + 128,
          'atomic',
          'PAUSE',
          50,
          1
        )
        .setOrigin(0.5, 0.5)
        .setAlpha(1)
        .setDepth(110)

      // save part
      this.position = [
        this.$scene.cameras.main.worldView.y + 180,
        this.$scene.cameras.main.worldView.y + 200,
      ]
      this.lastPosition = 0

      this.continueBtn = this.$scene.add
        .bitmapText(
          this.$scene.cameras.main.worldView.x + GAME_WIDTH / 4,
          this.position[0],
          'atomic',
          ' Continue ',
          16,
          1
        )
        .setTint(0xff3b00)
        .setDepth(110)
        .setOrigin(0.5, 0.5)

      this.saveGameBtn = this.$scene.add
        .bitmapText(
          this.$scene.cameras.main.worldView.x + GAME_WIDTH / 4,
          this.position[1],
          'atomic',
          '      Save ',
          16,
          1
        )
        .setTint(0xff3b00)
        .setDepth(300)
        .setOrigin(0.85, 0.5)

      this.head = this.$scene.add
        .image(
          this.$scene.cameras.main.worldView.x + GAME_WIDTH / 4 - 60,
          this.position[0],
          'head'
        )
        .setOrigin(0.5, 0.65)
        .setDisplaySize(15, 15)
        .setAlpha(1)
        .setDepth(110)
    }
  }

  choose () {
    gameState.player.chooseDone = true
    if (gameState.player.state.pause) {
      if (this.lastPosition === 1) {
        this.lastPosition = 0
      } else {
        this.lastPosition += 1
      }
      this.head.y = this.position[this.lastPosition]
      this.$scene.time.addEvent({
        delay: 300,
        callback: () => {
          gameState.player.chooseDone = false
        },
      })
    }
  }

  launch () {
    gameState.player.chooseDone = true
    if (gameState.player.state.pause) {
      if (this.lastPosition === 0) {
        gameState.player.state.pause = false
        this.$scene.physics.resume()
        gameState.player.anims.resume(gameState.player.anims.currentFrame)
        this.$scene.msgText.destroy()
        this.$scene.msg.destroy()
        this.continueBtn.destroy()
        this.saveGameBtn.destroy()
        this.head.destroy()
        this.firstTimestamp = new Date().getTime()
        this.$scene.time.addEvent({
          delay: 300,
          callback: () => {
            gameState.player.chooseDone = false
          },
        })
      }
      if (this.lastPosition === 1) {
        this.saveGame()
        setTimeout(() => {
          gameState.player.chooseDone = false
        }, 300)
      }
    }
  }

  saveGame () {
    console.log('gameState.player.inventory', gameState.player.inventory)
    if (gameState.boss1BattlePreventSave || gameState.bossFinalBattlePreventSave) {
      return
    }
    gameState.player.inventory.savedPositionX = gameState.player.x
    gameState.player.inventory.savedPositionY = gameState.player.y
    const s = JSON.stringify(gameState.player.inventory)
    localStorage.setItem('k438b', s)
    this.$scene.sound.play('melo')
    countPlayedTime(gameState.levelStartTimestamp)
  }

  loadGame () {
    console.log('gameState.player', gameState.player)
    const l = localStorage.getItem('k438b')
    if (JSON.parse(l)) {
      gameState.player.inventory = JSON.parse(l)
    }
    gameState.player.x = gameState.player.inventory?.savedPositionX ?? PLAYER_START_X
    gameState.player.y = gameState.player.inventory?.savedPositionY ?? PLAYER_START_Y
  }

  endMission () {
    if (!gameState.isTheEnd) {
      gameState.isTheEnd = true
      gameState.round = this.$scene.add
        .sprite(gameState.player.x, gameState.player.y, 'whitePixel')
        .setOrigin(0.5, 0.5)
        .setDepth(1000)
        .setDisplaySize(4096, 4096)
        .setAlpha(0)

      countPlayedTime(gameState.levelStartTimestamp)

      gameState.ambient1.stop()

      this.$scene.tween = this.$scene.tweens.add({
        targets: gameState.round,
        ease: 'Sine.easeInOut',
        duration: 1500,
        delay: 0,
        repeat: 0,
        yoyo: false,
        alpha: {
          getStart: () => 0,
          getEnd: () => 1,
        },
        onComplete: () => {
          gameState.lavaRise = null
          gameState.ambient1.stop()
          this.$scene.scene.start('endGame')
        },
      })
    }
  }
}
