import Manager from './Manager'
import gameState from '../state/game'
import PowerUp from '../sprites/PowerUp'

export default class PowerUpsManager extends Manager {
  init () {
    this.#setAnimations()
    this.#createPowerUps()
  }

  #setAnimations () {
    this.$scene.anims.create({
      key: 'powerupYellow',
      frames: this.$scene.anims.generateFrameNumbers('powerupYellow', {
        start: 0,
        end: 5,
        first: 0,
      }),
      frameRate: 10,
      yoyo: false,
      repeat: -1,
    })
    this.$scene.anims.create({
      key: 'powerupBlue',
      frames: this.$scene.anims.generateFrameNumbers('powerupBlue', {
        start: 0,
        end: 5,
        first: 0,
      }),
      frameRate: 10,
      yoyo: false,
      repeat: -1,
    })
    this.$scene.anims.create({
      key: 'powerupRed',
      frames: this.$scene.anims.generateFrameNumbers('powerupRed', {
        start: 0,
        end: 5,
        first: 0,
      }),
      frameRate: 10,
      yoyo: false,
      repeat: -1,
    })
    this.$scene.anims.create({
      key: 'powerupGreen',
      frames: this.$scene.anims.generateFrameNumbers('powerupGreen', {
        start: 0,
        end: 5,
        first: 0,
      }),
      frameRate: 10,
      yoyo: false,
      repeat: -1,
    })
    this.$scene.anims.create({
      key: 'powerUp',
      frames: this.$scene.anims.generateFrameNumbers('powerUp', {
        start: 0,
        end: 6,
        first: 0,
      }),
      frameRate: 10,
      yoyo: true,
      repeat: -1,
    })
  }

  #createPowerUps () {
    gameState.powerups = []
    gameState.map.objects[0].objects.forEach((element) => {
      if (gameState.player.inventory.powerUp[element.properties.id] === 0) {
        this.$scene[element.name] = new PowerUp(this.$scene, element.x, element.y - 16, {
          key: element.properties.key,
          name: element.properties.name,
          ability: element.properties.ability,
          text: element.properties.text,
          id: element.properties.id,
        })
        this.$scene[element.name]
          .setDisplayOrigin(0, 0)
          .animate(element.properties.powerup, true)
        this.$scene[element.name].body.setSize(16, 16).setAllowGravity(false)
        gameState.powerups.push(this.$scene[element.name])
      }
    })
    gameState.giveLifeGroup = []
  }

  getPowerUp (elm) {
    gameState.displayPowerUpMsg = true

    if (elm.state.ability === 'energy') {
      gameState.player.addEnergy()
      this.$scene.events.emit('setHealth', { life: gameState.player.inventory.life })
    } else if (elm.state.ability === 'speedfire') {
      gameState.player.addSpeedFire()
    } else if (
      elm.state.ability === 'missile' &&
      !gameState.player.inventory.boss1
    ) {
      gameState.displayPowerUpMsg = false
      return
    } else if (elm.state.ability === 'missile' && gameState.player.inventory.boss1) {
      gameState.player.addMissile()
    } else if (elm.state.ability === 'laser') {
      gameState.player.inventory[elm.state.ability] = true
      gameState.player.addLaser()
    } else if (elm.state.ability === 'swell') {
      gameState.player.inventory[elm.state.ability] = true
      gameState.player.addSwell()
    } else {
      gameState.player.inventory[elm.state.ability] = true
    }
    this.$scene.sound.play('powerUp')
    gameState.player.inventory.powerUp[elm.state.id] = 1

    this.$scene.notifyPowerUp(elm)

    elm.destroy()
  }
}
