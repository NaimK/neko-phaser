import Phaser from 'phaser'
import Manager from './Manager'
import gameState from '../state/game'

import Player from '../sprites/Player'
import { PLAYER_START_X, PLAYER_START_Y } from '../constants/positions'

export default class PlayerManager extends Manager {
  init () {
    this.#createPlayer()
    this.#setAnimations()
    this.#setWeapons()
    this.#setSounds()
    this.#setPlayerInWaterEffect()
    this.#setMask()
  }

  onUpdate () {
    this.#updateSonar()
  }

  #createPlayer () {
    gameState.player = new Player(this.$scene, PLAYER_START_X, PLAYER_START_Y, { key: 'player' })
    gameState.player.body.setSize(15, 35, 6, 11)
  }

  #setAnimations () {
    this.$scene.anims.create({
      key: 'walkShoot',
      frames: this.$scene.anims.generateFrameNumbers('playerShoot', {
        start: 0,
        end: 3,
        first: 0,
      }),
      frameRate: 10,
      repeat: -1,
    })
    this.$scene.anims.create({
      key: 'runShoot',
      frames: this.$scene.anims.generateFrameNumbers('playerShoot', {
        start: 0,
        end: 3,
        first: 0,
      }),
      frameRate: 5,
      repeat: -1,
    })
    this.$scene.anims.create({
      key: 'jumpBoost',
      frames: this.$scene.anims.generateFrameNumbers('idle', {
        start: 0,
        end: 4,
        first: 0,
      }),
      frameRate: 4,
      repeat: -1,
    })
    this.$scene.anims.create({
      key: 'stand',
      frames: this.$scene.anims.generateFrameNumbers('stand', {
        start: 1,
        end: 1,
        first: 1,
      }),
      frameRate: 5,
      repeat: -1,
    })
    this.$scene.anims.create({
      key: 'jump',
      frames: this.$scene.anims.generateFrameNumbers('jumpVertical', {
        start: 0,
        end: 4,
        first: 0,
      }),
      frameRate: 10,
      repeat: 0,
    })
    this.$scene.anims.create({
      key: 'jumpVertical',
      frames: this.$scene.anims.generateFrameNumbers('jumpVertical', {
        start: 0,
        end: 4,
        first: 0,
      }),
      frameRate: 10,
      repeat: 0,
    })
    this.$scene.anims.create({
      key: 'duck',
      frames: this.$scene.anims.generateFrameNumbers('stand', {
        start: 0,
        end: 0,
        first: 0,
      }),
      frameRate: 1,
      repeat: -1,
    })
    this.$scene.anims.create({
      key: 'shootup',
      frames: this.$scene.anims.generateFrameNumbers('shootUp', {
        start: 0,
        end: 0,
        first: 0,
      }),
      frameRate: 1,
      repeat: -1,
    })
    this.$scene.anims.create({
      key: 'morphingBall',
      frames: this.$scene.anims.generateFrameNumbers('morphingBall', {
        start: 0,
        end: 3,
        first: 0,
      }),
      frameRate: 16,
      repeat: -1,
    })
    this.$scene.anims.create({
      key: 'morphingBallIdle',
      frames: this.$scene.anims.generateFrameNumbers('morphingBall', {
        start: 0,
        end: 0,
        first: 0,
      }),
      frameRate: 1,
      repeat: -1,
    })
  }

  #setWeapons () {
    // player bullets
    gameState.player.bullets = this.$scene.physics.add.group({
      defaultKey: 'bullet',
      maxSize: 10,
      allowGravity: false,
      createIfNull: true,
    })
    this.$scene.anims.create({
      key: 'bull',
      frames: this.$scene.anims.generateFrameNumbers('bullet', {
        start: 0,
        end: 1,
        first: 0,
      }),
      frameRate: 10,
      repeat: -1,
    })
    this.$scene.anims.create({
      key: 'impact',
      frames: this.$scene.anims.generateFrameNumbers('impact', {
        start: 0,
        end: 4,
        first: 0,
      }),
      frameRate: 20,
      repeat: 0,
    })

    // player missiles
    gameState.player.missiles = this.$scene.physics.add.group({
      defaultKey: 'missile',
      maxSize: 1,
      allowGravity: false,
      createIfNull: true,
    })
    this.$scene.anims.create({
      key: 'missile',
      frames: this.$scene.anims.generateFrameNumbers('missile', {
        start: 0,
        end: 2,
        first: 0,
      }),
      frameRate: 10,
      repeat: -1,
    })

    // player swell
    gameState.player.swells = this.$scene.physics.add.group({
      defaultKey: 'swell',
      maxSize: 10,
      allowGravity: false,
      createIfNull: true,
    })
    this.$scene.anims.create({
      key: 'swell',
      frames: this.$scene.anims.generateFrameNumbers('swell', {
        start: 0,
        end: 7,
        first: 0,
      }),
      frameRate: 24,
      repeat: -1,
    })

    // player morphing bomb
    gameState.player.bombs = this.$scene.physics.add.group({
      defaultKey: 'bomb',
      maxSize: 3,
      allowGravity: false,
      createIfNull: true,
    })
    this.$scene.anims.create({
      key: 'bomb',
      frames: this.$scene.anims.generateFrameNumbers('bomb', {
        start: 0,
        end: 0,
        first: 0,
      }),
      frameRate: 1,
      repeat: -1,
    })
    this.$scene.anims.create({
      key: 'impactBomb',
      frames: this.$scene.anims.generateFrameNumbers('impact', {
        start: 0,
        end: 4,
        first: 0,
      }),
      frameRate: 10,
      repeat: 0,
    })

    // player laser
    gameState.player.lasers = this.$scene.physics.add.group({
      defaultKey: 'laser',
      maxSize: 10,
      allowGravity: false,
      createIfNull: true,
    })
  }

  #setSounds () {
    const walkSound = this.$scene.sound.add('run', { volume: 0.8 })
    let playWalkSound = false

    gameState.player.on('animationupdate', () => {
      if (
        gameState.player.anims.currentAnim.key === 'runShoot' &&
        !playWalkSound &&
        gameState.player.body.blocked.down
      ) {
        playWalkSound = true
        walkSound.play()
        this.$scene.time.addEvent({
          delay: 150,
          callback: () => {
            playWalkSound = false
          },
        })
      }
      if (
        gameState.player.anims.currentAnim.key === 'walkShoot' &&
        !playWalkSound &&
        gameState.player.body.blocked.down
      ) {
        playWalkSound = true
        walkSound.play()
        this.$scene.time.addEvent({
          delay: 250,
          callback: () => {
            playWalkSound = false
          },
        })
      }
    })
  }

  #setPlayerInWaterEffect () {
    gameState.waterAmbientMusic = this.$scene.sound.add('waterAmbient', { volume: 0.6 })
    gameState.solLayer.setTileLocationCallback(
      2,
      34,
      26,
      18,
      (e) => {
        if (e === gameState.player) {
          gameState.player.onWater = true
          gameState.player.setDepth(98)
          if (!gameState.waterAmbientMusic.isPlaying) {
            gameState.waterAmbientMusic.play()
          }
        }
      },
      this.$scene
    )
    gameState.solLayer.setTileLocationCallback(
      30,
      31,
      1,
      21,
      () => {
        gameState.player.onWater = false
        gameState.player.setDepth(105)
        gameState.waterAmbientMusic.stop()
      },
      this.$scene
    )
    gameState.solLayer.setTileLocationCallback(
      2,
      53,
      29,
      1,
      () => {
        gameState.player.onWater = false
        gameState.player.setDepth(105)
        gameState.waterAmbientMusic.stop()
      },
      this.$scene
    )
  }

  #setMask () {
    gameState.mask = this.$scene.make
      .graphics({ fillStyle: { color: 0xffffff }, add: false })
      .fillCircleShape(new Phaser.Geom.Circle(0, 6, 30))

    gameState.frontLayer.mask = new Phaser.Display.Masks.BitmapMask(this.$scene, gameState.mask)
    gameState.frontLayer.mask.invertAlpha = true
  }

  #updateSonar () {
    if (
      gameState.player.state.onMorphingBall &&
      gameState.player.inventory.morphingSonar
    ) {
      gameState.mask.x = gameState.player.x
      gameState.mask.y = gameState.player.y
    } else {
      gameState.mask.x = -300
      gameState.mask.y = -300
    }
  }
}
