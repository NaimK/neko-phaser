import gameState from '../state/game'

export default class Manager {
  $scene

  constructor () {
    this.$scene = gameState.scene
  }

  createAnimationFrom (key, mainAnimationName, config) {
    this.$scene.anims.create({
      key,
      frames: this.$scene.anims.generateFrameNumbers(mainAnimationName, {
        start: config.start ?? 0,
        end: config.end,
        first: config.first ?? 0,
      }),
      frameRate: config.frameRate ?? 10,
      yoyo: config.yoyo ?? false,
      repeat: config.repeat ?? -1,
    })
  }
}
