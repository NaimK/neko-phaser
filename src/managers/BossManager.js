import Manager from './Manager'
import gameState from '../state/game'
import Boss1 from '../sprites/enemies/Boss1'
import BossFinal from '../sprites/enemies/BossFinal'

export default class BossManager extends Manager {
  init () {
    this.#setAnimations()
    this.#setBoss1()
    this.#setFinalBoss()
  }

  #setAnimations () {
    // Boss 1
    this.createAnimationFrom('boss1walk', 'boss1walk', {
      start: 0,
      end: 13,
      frameRate: 40,
    })
    this.createAnimationFrom('boss1run', 'boss1run', {
      start: 0,
      end: 9,
      frameRate: 20,
    })
    this.createAnimationFrom('boss1hit', 'boss1hit', {
      start: 0,
      end: 8,
    })
    this.createAnimationFrom('boss1crouch', 'boss1crouch', {
      start: 0,
      end: 13,
      frameRate: 40,
    })
    this.createAnimationFrom('boss1attack', 'boss1attack', {
      start: 0,
      end: 8,
      frameRate: 15,
    })
    this.createAnimationFrom('boss1jump', 'boss1jump', {
      start: 0,
      end: 17,
    })
    // Final boss
    this.createAnimationFrom('bossFinalAttack', 'bossFinal', {
      start: 0,
      end: 7,
      frameRate: 16,
    })
    this.createAnimationFrom('bossFinalIntro', 'bossFinal', {
      start: 8,
      end: 8,
      frameRate: 1,
    })
  }

  #setBoss1 () {
    gameState.boss1started = false
    gameState.boss1BattlePreventSave = false
    gameState.bossMusic = this.$scene.sound.add('LetsPlayWithTheDemon', { volume: 0.2 })
    if (!gameState.player.inventory.boss1) {
      gameState.solLayer.setTileLocationCallback(
        78,
        77,
        1,
        3,
        (e) => {
          if (
            !gameState.boss1started &&
            e === gameState.player &&
            !gameState.player.inventory.boss1
          ) {
            this.#boss1BattlePrep()
          }
        },
      )
      gameState.solLayer.setTileLocationCallback(
        109,
        86,
        3,
        3,
        (e) => {
          if (e === gameState.player && !gameState.player.inventory.boss1) {
            this.#boss1Battle()
          }
        },
      )
    }
  }

  #boss1BattlePrep () {
    gameState.boss1started = true
    gameState.solLayer.setTileLocationCallback(78, 77, 1, 3, null)
    gameState.boss1wallfront = this.$scene.add
      .image(1912, 1340, 'boss1wallfront')
      .setDepth(2000)
      .setVisible(true)

    gameState.boss1 = new Boss1(this.$scene, 1930, 1350, { key: 'boss1run' })
    gameState.boss1.animate('boss1run')
    gameState.boss1.setAlpha(0)
    gameState.enemyGroup.push(gameState.boss1)
    gameState.boss1.body.setVelocity(0, 0).setEnable(false)
  }

  #boss1Battle () {
    if (!gameState.boss1started) {
      this.#boss1BattlePrep()
    }
    gameState.boss1BattlePreventSave = true
    gameState.solLayer.setTileLocationCallback(109, 86, 3, 3, null)
    gameState.boss1.body.setEnable()
    this.$scene.physics.add.collider(gameState.boss1, gameState.solLayer, null)
    gameState.boss1.setAlpha(1)
    gameState.boss1.animate('boss1run')
    gameState.boss1.intro = true
    gameState.boss1Door.body.enable = true
    gameState.boss1Door.setAlpha(1)
  }

  #setFinalBoss () {
    gameState.bossFinalReady = false
    gameState.bossFinalStarted = false
    gameState.bossFinalBattlePreventSave = false

    if (!gameState.player.inventory.bossFinal) {
      gameState.solLayer.setTileLocationCallback(
        70,
        127,
        8,
        1,
        (e) => {
          if (
            !gameState.bossFinalReady &&
              e === gameState.player &&
              !gameState.player.inventory.bossFinal
          ) {
            this.#bossFinalBattlePrep()
          }
        },
        this.$scene
      )
      gameState.solLayer.setTileLocationCallback(
        1,
        188,
        116,
        1,
        (e) => {
          if (
            !gameState.bossFinalReady &&
              e === gameState.player &&
              !gameState.player.inventory.bossFinal
          ) {
            this.#bossFinalBattlePrep()
          }
        },
        this.$scene
      )
    }
  }

  #bossFinalBattlePrep () {
    gameState.bossFinalReady = true
    gameState.bossFinal = new BossFinal(this.$scene, 1250, 2960, { key: 'bossFinal' })
    gameState.bossFinal.animate('bossFinalIntro')
    this.$scene.physics.add.collider(gameState.bossFinal, gameState.solLayer, null)
    this.$scene.physics.add.overlap(
      gameState.bossFinal,
      gameState.player,
      (elm) => this.$scene.damages.playerIsHit(elm),
      null,
      this.$scene
    )
    this.$scene.physics.add.overlap(
      [
        gameState.player.bullets,
        gameState.player.swells,
        gameState.player.missiles,
        gameState.player.lasers,
      ],
      gameState.bossFinal,
      (elm, bull) => this.$scene.damages.enemyIsHit(bull, elm, gameState.player),
      null,
      gameState.player
    )
    gameState.bossFinal.flames = this.$scene.physics.add.group({
      defaultKey: 'fireball',
      maxSize: 900,
      allowGravity: false,
      createIfNull: true,
    })
    this.$scene.physics.add.collider(
      gameState.bossFinal.flames,
      gameState.solLayer,
      (elm) => {
        gameState.bossFinal.stopFlame(elm)
      },
      null
    )
    this.$scene.physics.add.overlap(
      gameState.bossFinal.flames,
      gameState.player,
      () => gameState.player.handleLava(),
      null,
      gameState.player
    )
    gameState.boss1dead = this.$scene.add.image(925, 3010, 'boss1dead').setDepth(100)
    gameState.boss1dead.flipX = true
    gameState.solLayer.setTileLocationCallback(
      59,
      188,
      1,
      6,
      (e) => {
        if (
          e === gameState.player &&
          !gameState.player.inventory.bossFinal &&
          !gameState.bossFinalStarted
        ) {
          this.bossFinalBattleStart()
        }
      },
      this
    )
    gameState.solLayer.setTileLocationCallback(70, 127, 8, 1, null)
    gameState.solLayer.setTileLocationCallback(1, 188, 116, 1, null)
  }

  bossFinalBattleStart () {
    gameState.bossFinalBattlePreventSave = true
    gameState.bossFinal.playBossMusic()
    gameState.bossFinal.playRoar('cri1')
    setTimeout(() => {
      gameState.bossFinal.animate('bossFinalAttack')
    }, 700)
    this.$scene.time.addEvent({
      delay: 700,
      callback: () => {
        if (gameState.bossFinal.y < 2810) {
          gameState.bossFinal.body.velocity.y = 0
        } else if (gameState.bossFinal.y > 2900) {
          gameState.bossFinal.body.velocity.y = -300
        } else {
          gameState.bossFinal.body.velocity.y = 0
        }
      },
    })
    setTimeout(() => { gameState.bossFinalStarted = true }, 3000)
  }
}
