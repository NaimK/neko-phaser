import Manager from './Manager'
import gameState from '../state/game'

export default class CollisionsManager extends Manager {
  init () {
    this.#setColliders()
    this.#setOverlaps()
  }

  #setColliders () {
    gameState.solLayer.setCollisionByProperty({ collides: true })

    this.$scene.physics.add.collider(gameState.player, gameState.solLayer, null)
    this.$scene.physics.add.collider(gameState.doorGroup, gameState.player, null)
    this.$scene.physics.add.collider(gameState.enemyGroup, gameState.solLayer, null)
    this.$scene.physics.add.collider(gameState.enemyGroup, gameState.doorGroup, null)
    this.$scene.physics.add.collider(gameState.lavaGroup, gameState.solLayer, null)
    this.$scene.physics.add.collider(
      [gameState.player.bullets, gameState.player.swells],
      gameState.solLayer,
      gameState.player.bulletKill,
      null,
      gameState.player
    )
    this.$scene.physics.add.collider(
      gameState.player.missiles,
      gameState.solLayer,
      gameState.player.missileKill,
      null,
      gameState.player
    )
    this.$scene.physics.add.collider(
      gameState.player.lasers,
      gameState.solLayer,
      gameState.player.laserKill,
      null,
      gameState.player
    )
    this.$scene.physics.add.collider(
      [gameState.player.bullets, gameState.player.swells],
      gameState.doorGroup,
      (bull, d) => gameState.player.bulletKill(d),
      null,
      gameState.player.bullets
    )
    this.$scene.physics.add.collider(
      gameState.player.lasers,
      gameState.doorGroup,
      (bull, d) => gameState.player.laserKill(d),
      null,
      gameState.player.lasers
    )
    this.$scene.physics.add.collider(
      gameState.player.missiles,
      gameState.doorGroup,
      (d, miss) => this.$scene.environment.openDoor(d, miss),
      null,
      this
    )
    this.$scene.physics.add.collider(
      gameState.elevatorGroup,
      [gameState.player, gameState.player.bullets],
      (elm) => elm.handleElevator(gameState.player),
      null,
      this
    )
  }

  #setOverlaps () {
    this.$scene.physics.add.overlap(
      gameState.lavaGroup,
      gameState.player,
      () => gameState.player.handleLava(),
      null,
      gameState.player
    )
    this.$scene.physics.add.overlap(
      gameState.giveLifeGroup,
      gameState.player,
      (elm) => gameState.player.getLife(elm),
      null,
      gameState.player
    )
    this.$scene.physics.add.overlap(
      gameState.powerups,
      gameState.player,
      (elm) => this.$scene.getPowerUp(elm),
      null,
      this.$scene
    )
    this.$scene.physics.add.overlap(
      gameState.enemyGroup,
      gameState.player,
      (elm) => this.$scene.damages.playerIsHit(elm),
      null,
      this.$scene
    )
    this.$scene.physics.add.overlap(
      [
        gameState.player.bullets,
        gameState.player.swells,
        gameState.player.missiles,
        gameState.player.lasers,
      ],
      gameState.enemyGroup,
      (elm, bull) => this.$scene.damages.enemyIsHit(bull, elm, gameState.player),
      null,
      gameState.player
    )
  }
}
