import Manager from './Manager'
import gameState from '../state/game'
import { GAME_HEIGHT, GAME_WIDTH } from '../constants/sizes'

export default class MapManager extends Manager {
  init () {
    this.#setMap()
    this.#setBackgrounds()
    this.#setLayers()
  }

  #setMap () {
    gameState.map = this.$scene.make.tilemap({ key: 'mapLevel1', tileWidth: 16, tileHeight: 16 })
    gameState.tileset = gameState.map.addTilesetImage('tileground', 'tiles', 16, 16)
  }

  #setBackgrounds () {
    // parralax backgrounds
    this.$scene.add
      .image(0, 0, 'para_back')
      .setDepth(0)
      .setScrollFactor(0.1)
      .setOrigin(0, 0)
      .setDisplaySize(GAME_WIDTH, GAME_HEIGHT)

    this.$scene.add
      .image(0, 0, 'para_middle')
      .setDepth(3)
      .setScrollFactor(0.5)
      .setOrigin(0, 0)
      .setDisplaySize(2048, 1024)

    this.$scene.add
      .image(0, 880, 'bgLava')
      .setDepth(5)
      .setScrollFactor(0.5)
      .setOrigin(0, 0)
      .setDisplaySize(1600, 1200)
  }

  #setLayers () {
    gameState.backLayer = gameState.map
      .createLayer('back', gameState.tileset, 0, 0)
      .setDepth(4)

    gameState.middleLayer = gameState.map
      .createLayer('middle', gameState.tileset, 0, 0)
      .setDepth(5)

    gameState.middleLayer2 = gameState.map
      .createLayer('middle2', gameState.tileset, 0, 0)
      .setDepth(10)

    gameState.statue = gameState.map
      .createLayer('statue', gameState.tileset, 0, 0)
      .setDepth(98)

    gameState.eau = gameState.map
      .createLayer('eau', gameState.tileset, 0, 0)
      .setDepth(99)

    gameState.solLayer = gameState.map
      .createLayer('sol', gameState.tileset, 0, 0)
      .setDepth(100)

    gameState.frontLayer = gameState.map
      .createLayer('front', gameState.tileset, 0, 0)
      .setDepth(106)
  }
}
