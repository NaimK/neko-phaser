import Phaser from 'phaser'
import { GAME_WIDTH, GAME_HEIGHT } from './constants/sizes'
import scenes from './scenes'
// import CustomDebugger from './plugins/CustomDebugger'
import DevMode from './plugins/DevMode'

const config = {
  type: Phaser.AUTO,
  parent: 'cypher-neko',
  pixelArt: true,
  scale: {
    autoCenter: Phaser.Scale.CENTER_BOTH,
    autoRound: true,
    mode: Phaser.Scale.FIT,
    width: GAME_WIDTH,
    height: GAME_HEIGHT,
  },
  physics: {
    default: 'arcade',
    arcade: {
      gravity: { y: 100 },
      // debug: true,
    },
  },
  plugins: {
    scene: [
      {
        plugin: DevMode,
        key: 'DevMode',
        start: true,
      },
      // {
      //   key: 'CustomDebugger', plugin: CustomDebugger, start: true,
      // },
    ],
  },
  scene: scenes,
}

// eslint-disable-next-line no-unused-vars
const game = new Phaser.Game(config)
